# lasermaze

University of Tartu, Software Project 2018

Daniel Nael, Ott Saar, Raigo K�vask, Carola Kesk�la

[Link to browse all Wiki entries](https://bitbucket.org/carolakeskyla/lasermaze/wiki/browse/)

### Project description 

Within this project a program to control laser obstacle course games is created. The project is created within University of Tartu's Software Project course. 
Our client is University of Tartu's Physics Institute and Photonics Club. 

A laser obstacle course (laser maze) is a form of entertainment, where a room with lasers, light detectors and smoke is used to create beams of light which have to be avoided 
by a participating player in order to win. An electronic stopper is used to measure the time and mistakes it took the player to successfully complete the course. 
Interrupting the light beams a custom amount of times results in a failure to complete the course.

This project consists of two communicating programs - one for Arduino, which controls peripheral devices and handles input from light sensors and buttons, and another for PC,
which harbours a user-interface with various settings. 

Mechanical tasks regarding the physical laser maze course itself is out of our project's scope. Tasks like how the lasers are positioned or the stability of the construction
are handled by a team of students in University of Tartu's Physics Institute. 