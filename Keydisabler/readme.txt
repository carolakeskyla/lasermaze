Disable.bat fail muudab nuppude v��rtused �mber.
V��rtuste kinnitamiseks peab kasutaja korra arvutist v�lja logima.
Et taastada nuppude endised v��rtused tuleb k�ivitada Enable.bat
administraatorina, ning kasutaja peab uuesti v�lja logima kinnitamiseks.

Allpool on n�ha, mis nuppudeks muudab Disable.bat nuppude v��rtused.

00000000
00000000
07000000
4be01de0 = left ctrl -> left arrow
4be01d00 = right ctrl -> left arrow
4de03800 = left alt -> right arrow
4de038e0 = right alt -> right arrow
4be00f00 = tab -> left arrow
4be00100 = escape -> left arrow
00000000