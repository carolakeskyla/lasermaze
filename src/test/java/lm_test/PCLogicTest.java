package lm_test;

import org.junit.*;
import lm_main.*;

public class PCLogicTest 
{
	private PCLogic pcLogic;
	
	@Before
	public void setUp()
	{
		pcLogic = new PCLogic(new MockupUI());		
	}
	
	@Test
	public void createGameTest()
	{
		Assert.assertNull(pcLogic.getCurrentGame());
		pcLogic.makeGame();
		Assert.assertNotNull(pcLogic.getCurrentGame());
		pcLogic.endGame();
	}
	
	@Test
	public void defaultPlayerNameTest()
	{
		pcLogic.makeGame();
		Assert.assertEquals(Globals.name, pcLogic.getCurrentGame().getPlayerName());
		pcLogic.endGame();
	}
	
	@Test
	public void setPlayerNameTest()
	{
		PCLogic.setPlayerName("Test");
		pcLogic.makeGame();
		Assert.assertEquals("Test", pcLogic.getCurrentGame().getPlayerName());
		pcLogic.endGame();
	}
	
	@Test
	public void maxMistakesTest()
	{
		pcLogic.makeGame();
		Assert.assertEquals(Globals.maxMistakes, pcLogic.getCurrentGame().getMaxMistakes());
		pcLogic.endGame();
	}
	
	@Test
	public void setMaxMistakesTest()
	{
		pcLogic.makeGame();
		PCLogic.setMaxMistakes(10);
		Assert.assertEquals(10, pcLogic.getCurrentGame().getMaxMistakes());
		pcLogic.endGame();
	}
	
	@Test
	public void startGameTest()
	{
		pcLogic.makeGame();
		pcLogic.startGame();
		Assert.assertEquals("Running", pcLogic.getGameStatus());
		pcLogic.endGame();
	}
	
	@Test
	public void pauseGameTest()
	{
		pcLogic.makeGame();
		pcLogic.startGame();
		pcLogic.pauseGame();
		Assert.assertEquals("Paused", pcLogic.getGameStatus());
		pcLogic.endGame();
	}
	
	@Test
	public void endGameTest()
	{
		pcLogic.makeGame();
		pcLogic.startGame();
		pcLogic.endGame();
		Assert.assertEquals("Stopped", pcLogic.getGameStatus());
		pcLogic.endGame();
		
		pcLogic.makeGame();
		pcLogic.startGame();
		pcLogic.pauseGame();
		pcLogic.endGame();
		Assert.assertEquals("Stopped", pcLogic.getGameStatus());
		pcLogic.endGame();
	}
}
