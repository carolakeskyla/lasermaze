package lm_test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.control.LabeledMatchers.hasText;

import java.sql.Time;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import lm_main.Game;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.service.query.NodeQuery;

import javafx.scene.input.KeyCode;
import javafx.scene.Parent;
import javafx.stage.Stage;
import lm_application.GameMasterViewController;
import lm_application.LaserMazeUI;
import lm_application.PlayerViewController;
import lm_main.Globals;
import lm_main.Sound;
import org.testfx.toolkit.ApplicationLauncher;

import org.junit.Test;

public class SQTestsPart2 extends ApplicationTest
{
    private final double secondaryScreenWidth = 100;
    private final double secondaryScreenHeight = 100;
    private LaserMazeUI sceneRoot = new LaserMazeUI();
    private PlayerViewController pvc = new PlayerViewController(sceneRoot);
    private GameMasterViewController gmvContr = new GameMasterViewController(sceneRoot);
    private Parent pvcRoot;
    private Parent GMV;
    private Stage stage2;
    private Stage mainStage;

    @Override public void start(Stage stage) throws Exception
    {
        stage2 = stage;
        sceneRoot.start(stage);
    }

    private void enterPlayersName()
    {
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.U);
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.L);
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.N);
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.O);
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.ENTER);
    }

    @Test
    public void TC9 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        clickOn("#startButton");

        sleep(3, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().makeEmptySensors();

        sleep(2, TimeUnit.SECONDS);
        sceneRoot.getPCLogic().makeATestMistake(22);

        assertEquals(true, Sound.isPlaying(Sound.Sounds.klaxon1));
        assertEquals(1, sceneRoot.getPCLogic().getCurrentGame().getMistakesMade(22));
        assertEquals(true, sceneRoot.getPCLogic().getCurrentGame().isRunning());
    }

    @Test
    public void TC10 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        // Click on start button.
        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);
        sceneRoot.getPCLogic().makeEmptySensors();

        sleep(2, TimeUnit.SECONDS);
        sceneRoot.getPCLogic().makeATestMistake(22);

        sleep(2, TimeUnit.SECONDS);
        sceneRoot.getPCLogic().makeATestMistake(23);

        assertEquals(1, sceneRoot.getPCLogic().getCurrentGame().getMistakesMade(22));
        assertEquals(1, sceneRoot.getPCLogic().getCurrentGame().getMistakesMade(23));

        sleep(1, TimeUnit.SECONDS);

        clickOn("#stopButton");

        sleep(2, TimeUnit.SECONDS);
        assertEquals(2, sceneRoot.getPCLogic().getLastGame().getTotalMistakesMade());
    }

    @Test
    public void TC11 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        // Click on start button.
        clickOn("#startButton");

        assertEquals(true, Sound.isPlaying(Sound.Sounds.activated));

        sleep(1, TimeUnit.SECONDS);

        clickOn("#stopButton");

        assertEquals(true, Sound.isPlaying(Sound.Sounds.success));

    }

    @Test
    public void TC15 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        // Click on start button.
        clickOn("#startButton");

        sleep(1, TimeUnit.SECONDS);
        sceneRoot.getPCLogic().makeEmptySensors();

        sleep(2, TimeUnit.SECONDS);

        clickOn("#sensor2");

        sleep(2, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().makeATestMistake(22);

        sleep(2, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().makeATestMistake(23);

        sleep(2, TimeUnit.SECONDS);

        assertEquals(1, sceneRoot.getPCLogic().getCurrentGame().getMistakesMade(22));
        assertEquals(0, sceneRoot.getPCLogic().getCurrentGame().getMistakesMade(23));

        sleep(1, TimeUnit.SECONDS);

        clickOn("#stopButton");

        sleep(2, TimeUnit.SECONDS);
        assertEquals(1, sceneRoot.getPCLogic().getLastGame().getTotalMistakesMade());


    }

    @Test
    public void TC16 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        // Click on start button.
        clickOn("#startButton");

        sleep(1, TimeUnit.SECONDS);
        sceneRoot.getPCLogic().makeEmptySensors();

        sleep(2, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().makeATestMistake(22);

        sleep(2, TimeUnit.SECONDS);

        clickOn("#stopButton");

        sleep(9, TimeUnit.SECONDS);

        assertEquals(null, sceneRoot.getPCLogic().getCurrentGame());
        assertEquals(false, Sound.isPlaying(Sound.Sounds.success));
    }

    @Test
    public void TC17 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        // Click on start button.
        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);

        assertEquals(true, sceneRoot.getPCLogic().getCurrentGame().isRunning());
        assertEquals("ulno", sceneRoot.getPCLogic().getCurrentGame().getPlayerName().toLowerCase());
    }

    @Test
    public void TC18 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        sleep(1, TimeUnit.SECONDS);

        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().getCurrentGame().mistake(22);

        clickOn("#stopButton");

        boolean moreThan2Sec = sceneRoot.getPCLogic().getLastGame().timeTaken() > 2000;

        assertEquals(1, sceneRoot.getPCLogic().getLastGame().getMistakesMade(22));
        assertEquals("ulno", sceneRoot.getPCLogic().getLastGame().getPlayerName().toLowerCase());
        assertEquals(true, moreThan2Sec);
    }

    @Test
    public void TC19 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        sleep(1, TimeUnit.SECONDS);

        clickOn("#maxMistakes");

        sleep(2, TimeUnit.SECONDS);

        press(KeyCode.BACK_SPACE);

        sleep(2, TimeUnit.SECONDS);

        press(KeyCode.DIGIT2);

        sleep(2, TimeUnit.SECONDS);

        clickOn("#saveSettingsButton");

        sleep(2,TimeUnit.SECONDS);

        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);

        clickOn("#maxMistakes");

        sleep(2, TimeUnit.SECONDS);

        press(KeyCode.BACK_SPACE);

        sleep(2, TimeUnit.SECONDS);

        press(KeyCode.DIGIT5);

        sleep(2, TimeUnit.SECONDS);

        clickOn("#saveSettingsButton");

        sleep(2,TimeUnit.SECONDS);

        assertEquals(2, Globals.maxMistakes);
    }

    @Test
    public void TC20 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        sleep(1, TimeUnit.SECONDS);

        clickOn("#maxMistakes");

        sleep(2, TimeUnit.SECONDS);

        press(KeyCode.BACK_SPACE);

        sleep(2, TimeUnit.SECONDS);

        press(KeyCode.DIGIT3);

        sleep(2, TimeUnit.SECONDS);

        clickOn("#saveSettingsButton");

        sleep(2, TimeUnit.SECONDS);

        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().makeEmptySensors();

        sleep(2, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().makeATestMistake(22);

        sleep(2, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().makeATestMistake(22);

        sleep(2, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().makeATestMistake(22);

        assertEquals(false, sceneRoot.getPCLogic().getCurrentGame().isRunning());
    }
}
