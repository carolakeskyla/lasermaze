package lm_test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.control.LabeledMatchers.hasText;

import java.sql.Time;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import lm_main.Game;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.service.query.NodeQuery;

import javafx.scene.input.KeyCode;
import javafx.scene.Parent;
import javafx.stage.Stage;
import lm_application.GameMasterViewController;
import lm_application.LaserMazeUI;
import lm_application.PlayerViewController;
import lm_main.Globals;
import lm_main.Sound;
import org.testfx.toolkit.ApplicationLauncher;

import org.junit.Test;

public class SQTests extends ApplicationTest
{
    private final double secondaryScreenWidth = 100;
    private final double secondaryScreenHeight = 100;
    private LaserMazeUI sceneRoot = new LaserMazeUI();
    private PlayerViewController pvc = new PlayerViewController(sceneRoot);
    private GameMasterViewController gmvContr = new GameMasterViewController(sceneRoot);
    private Parent pvcRoot;
    private Parent GMV;
    private Stage stage2;
    private Stage mainStage;

    @Override public void start(Stage stage) throws Exception
    {
        stage2 = stage;
        sceneRoot.start(stage);
    }

    private void enterPlayersName()
    {
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.U);
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.L);
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.N);
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.O);
        sleep(10, TimeUnit.MILLISECONDS);
        press(KeyCode.ENTER);
    }

    @Test
    public void TC1 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        // Click on start button.
        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);

        assertEquals(true, sceneRoot.getPCLogic().getCurrentGame().isRunning());
    }

    @Test
    public void TC2()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        // Press on the start button.
        clickOn("#startButton");

        assertEquals(false, sceneRoot.getPCLogic().getCurrentGame().isPaused());

        sleep(1, TimeUnit.SECONDS);

        // Press on the pause button.
        clickOn("#pauseButton");

        sleep(1, TimeUnit.SECONDS);

        assertEquals(true, sceneRoot.getPCLogic().getCurrentGame().isPaused());
    }

    @Test
    public void TC3()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        clickOn("#startButton");

        assertEquals(false, sceneRoot.getPCLogic().getCurrentGame().isPaused());

        sleep(1, TimeUnit.SECONDS);

        clickOn("#pauseButton");

        sleep(1, TimeUnit.SECONDS);

        assertEquals(true, sceneRoot.getPCLogic().getCurrentGame().isPaused());

        sleep(1, TimeUnit.SECONDS);

        clickOn("#startButton");

        assertEquals(true, sceneRoot.getPCLogic().getCurrentGame().isRunning());
    }

    @Test
    public void TC4 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        clickOn("#startButton");

        sleep(1, TimeUnit.SECONDS);

        assertEquals(true, sceneRoot.getPCLogic().getCurrentGame().isRunning());

        sleep(1, TimeUnit.SECONDS);

        clickOn("#stopButton");

        assertEquals(null, sceneRoot.getPCLogic().getCurrentGame());
    }

    @Test
    public void TC5 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);


        clickOn("#stopButton");

        sleep(1, TimeUnit.SECONDS);

        boolean moreThan2Sec = sceneRoot.getPCLogic().getLastGame().timeTaken() > 2000;

        assertEquals(true, moreThan2Sec);
    }

    @Test
    public void TC6 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);


        clickOn("#stopButton");

        sleep(1, TimeUnit.SECONDS);

        boolean moreThan2Sec = sceneRoot.getPCLogic().getLastGame().timeTaken() > 2000;

        assertEquals(true, moreThan2Sec);
        assertEquals("ulno", sceneRoot.getPCLogic().getLastGame().getPlayerName().toLowerCase());
    }

    @Test
    public void TC7 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);

        clickOn("#pauseButton");
        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);


        clickOn("#stopButton");

        boolean moreThan4Sec = sceneRoot.getPCLogic().getLastGame().timeTaken() > 4000;

        assertEquals(true, moreThan4Sec);
        assertEquals("ulno", sceneRoot.getPCLogic().getLastGame().getPlayerName().toLowerCase());

        sleep(2, TimeUnit.SECONDS);
    }

    @Test
    public void TC8 ()
    {
        // Wait for the main view.
        sleep(1, TimeUnit.SECONDS);

        // Press any key.
        press(KeyCode.P);

        // Enter player's name.
        enterPlayersName();

        clickOn("#startButton");

        sleep(2, TimeUnit.SECONDS);

        sceneRoot.getPCLogic().getCurrentGame().mistake(22);

        clickOn("#stopButton");

        assertEquals(null, sceneRoot.getPCLogic().getCurrentGame());
        assertEquals(1, sceneRoot.getPCLogic().getLastGame().getMistakesMade(22));

        sleep(2, TimeUnit.SECONDS);
    }
    
}
