package lm_test;

import javafx.scene.image.Image;
import lm_application.UI;

public class MockupUI implements UI
{
	@Override
	public void arduinoDisconnected(boolean value) {}

	@Override
	public void setTime() {}

	@Override
	public void setSensor(int sensorID, boolean currentVal, int currentMistakes) {}

	@Override
	public void sendCamInfo(Image img) {}

	@Override
	public void normalizeState() {}

	@Override
	public void gameStopped() {}

	@Override
	public void gamePaused() {}

	@Override
	public void gameStarted() {}
}
