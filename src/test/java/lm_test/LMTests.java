package lm_test;

import lm_arduino.Sensor;
import lm_main.PCLogic;
import org.junit.Before;
import org.junit.Test;

public class LMTests {
    private PCLogic pcLogic;
    @Before
    public void setUp()
    {
        pcLogic = new PCLogic(new MockupUI());
    }
    @Test
    public void Test1(){
        Sensor s = new Sensor(1, true);
        pcLogic.AddSensor(s);
        var mistakeMade= pcLogic.mistakeMade(s);
        assert !mistakeMade;
        try {
            var start = System.currentTimeMillis();
            Thread.sleep((long) pcLogic.sensorDelay+1l);
            System.out.println(System.currentTimeMillis() - start);
            var mistakeMade2 = pcLogic.mistakeMade(s);
            assert mistakeMade2;
        } catch (InterruptedException e) {
            assert false;
        }

    }
    @Test
    public void Test2(){
        pcLogic.startGame();
        assert pcLogic.getGameStatus().equals("Running");
        pcLogic.pauseGame();
        assert pcLogic.getGameStatus().equals("Paused");
        pcLogic.startGame();
        pcLogic.endGame();
        assert pcLogic.getGameStatus().equals("Stopped");
    }
    @Test
    public void Test3(){
        var sens = pcLogic.getSensorFromMap(2);
        var sens2 = pcLogic.getSensorFromMap(2);
        assert sens == sens2;
    }

}
