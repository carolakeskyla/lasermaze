package lm_test;
import static org.junit.Assert.assertEquals;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.control.LabeledMatchers.hasText;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.testfx.framework.junit.ApplicationTest;
import org.testfx.service.query.NodeQuery;

import javafx.scene.input.KeyCode;
import javafx.scene.Parent;
import javafx.stage.Stage;
import lm_application.GameMasterViewController;
import lm_application.LaserMazeUI;
import lm_application.PlayerViewController;
import lm_main.Globals;
import lm_main.Sound;

import org.junit.Test;

public class GameMasterViewTest extends ApplicationTest {

	private double secondaryScreenWidth = 100;
	private double secondaryScreenHeight = 100;
	LaserMazeUI sceneRoot = new LaserMazeUI();
	PlayerViewController pvc = new PlayerViewController(sceneRoot);
	GameMasterViewController gmvContr = new GameMasterViewController(sceneRoot);
	Parent pvcRoot;
	Parent GMV;
	Stage stage2;
	private Stage mainStage;
    @Override public void start(Stage stage) throws Exception{
    	stage2 = stage;
    	sceneRoot.start(stage);
    }
    // Use case 1
    @Test public void gameStartPauseStop() throws TimeoutException {
    	sleep(6, TimeUnit.SECONDS);
        assertEquals(sceneRoot.getPCLogic().getCurrentGame(), null);
        press(KeyCode.I);
        sleep(1, TimeUnit.MILLISECONDS);
        press(KeyCode.I);
        sleep(1, TimeUnit.MILLISECONDS);
        press(KeyCode.ENTER);
        sleep(1, TimeUnit.SECONDS);
        clickOn("#startButton");
        sleep(1, TimeUnit.SECONDS);
        System.out.println(sceneRoot.getPCLogic());
        assertEquals(sceneRoot.getPCLogic().getCurrentGame().isRunning(), true);
        clickOn("#pauseButton");
        sleep(1, TimeUnit.SECONDS);
        assertEquals(sceneRoot.getPCLogic().getCurrentGame().isPaused(), true);
        clickOn("#stopButton");
        sleep(1, TimeUnit.SECONDS);
        assertEquals(sceneRoot.getPCLogic().getCurrentGame(), null);
        System.out.println(sceneRoot.getPCLogic());
        sceneRoot.getPCLogic().close();
        sleep(1, TimeUnit.SECONDS);
    }
    
    // Use case 2
    @Test public void gameStartSound() throws TimeoutException {
    	sleep(6, TimeUnit.SECONDS);
        clickOn("#startButton");
        sleep(1, TimeUnit.SECONDS);
        assertEquals(sceneRoot.getPCLogic().getCurrentGame().isRunning(), true);
        assertEquals(Sound.isPlaying(Sound.Sounds.activated), true);
        sleep(1, TimeUnit.SECONDS);
        clickOn("#stopButton");
        sceneRoot.getPCLogic().close();
        sleep(1, TimeUnit.SECONDS);
    }
    /*// Use case 3
    @Test public void gamePause() throws TimeoutException {
    	sleep(6, TimeUnit.SECONDS);
        clickOn("#startButton");
        assertEquals(sceneRoot.getPCLogic().getCurrentGame().isRunning(), true);
        clickOn("#pauseButton");
        long time = sceneRoot.getPCLogic().getCurrentGame().timeTaken();
        sleep(1, TimeUnit.SECONDS);
        assertEquals(true, sceneRoot.getPCLogic().getCurrentGame().timeTaken() - time < 0.01);
        int mistakes = sceneRoot.getPCLogic().getCurrentGame().getTotalMistakesMade();
        sceneRoot.getPCLogic().getCurrentGame().mistake(22);
        assertEquals(mistakes, sceneRoot.getPCLogic().getCurrentGame().getTotalMistakesMade());
        sleep(1, TimeUnit.SECONDS);
        clickOn("#stopButton");
        sceneRoot.getPCLogic().close();
        sleep(1, TimeUnit.SECONDS);
    }
    // Use case 29
    @Test public void playerNameIsChanged() {
    	sleep(6, TimeUnit.SECONDS);
    	sceneRoot.setScreen("player");
        // expect:
    	//mainStage = sceneRoot.getSecondaryStage();
    	NodeQuery nq = lookup("#playerNameField");
    	press(KeyCode.I);
    	sleep(1,TimeUnit.MILLISECONDS);
    	press(KeyCode.A);
    	sleep(1,TimeUnit.MILLISECONDS);
    	press(KeyCode.M);
    	press(KeyCode.ENTER);
    	assertEquals("iam", Globals.name);
    }*/

    /*@Test public void should_click_on_button() {
        // when:

        // then:
        verifyThat(".button", hasText("clicked!"));
    }*/
}
