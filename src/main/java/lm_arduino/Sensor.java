package lm_arduino;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Raigo Kovask
 * @date 27.09.2018
 * @usage
 * Sensor class has been made to make
 * communication between PCLogic and 
 * Communicator easier
 */
@Entity
@Table(name = "Sensor")
public class Sensor{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name = "sensorID")
	private int sensorID;
	@Transient
	private boolean sensorState;
	@Transient
	private boolean usedInGame;
	@Column(name = "mistakes")
	private int mistakes;


	public Sensor(int sensorID, boolean sensorState) {
		this.sensorID = sensorID;
		this.sensorState = sensorState;
		usedInGame = true;
	}
	
	public Sensor(int sensorID, int mistakes)
	{
		this.sensorID = sensorID;
		this.mistakes = mistakes;
	}
	
	public void setUseInGame(boolean usedInGame){ this.usedInGame = usedInGame; }
	public void setSensorState(boolean sensorState) {
		this.sensorState = sensorState;
	}

	/**
	 * @return sensor pin number.
	 */
	public int getSensorID() {
		return sensorID;
	}
	/**
	 *
	 * 
	 * @return current state of sensor.
	 */
	public boolean isSensorActive() {
		return sensorState;
	}

	public boolean isUsedInGame() { return usedInGame; }

	@Override
	public boolean equals(Object other) {
	    if(other == null) {
	    	return false;
	    } else {
	    	if(other instanceof Sensor) {
	    		return ((Sensor)other).getSensorID() == this.sensorID;
	    	} else {
	    		return false;
	    	}
	    }
	}

	@Override
	public String toString() {
		return "Sensor [sensorID=" + sensorID + ", sensorState=" + sensorState + "]";
	}
}
