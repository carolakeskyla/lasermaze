package lm_arduino;

import com.fazecast.jSerialComm.*;

import lm_main.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.PortUnreachableException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @authors Team Laser Maze
 * @usage
 * Communicator class has default port of 20.
 * It is essential, that you set correct port 
 * by yourself.
 * 
 * It extends Thread, so it is runnable. First
 * set the port by constructor or with setPort
 * method. Then call the run() method and listen
 * hasnext() method for incoming sensor info. To
 * get the sensor call nextSensor() method.
 */
public class Communicator extends Thread {

	private enum currentState{Idle, Sensor, Button, SensorResponse};
    private SerialPort port;
    private InputStream stream;
    private Queue<Sensor> IncomingSensorBuffer;
    private boolean openport = false;
    public boolean isChecking = false;
    private currentState comState = currentState.Idle;
	private String receivedMessage = "";
	private String lastMessage = "a";
	private boolean activated = true;
    
    public Communicator(){
    	IncomingSensorBuffer = new LinkedList<Sensor>();
    	this.start();
    	Logger.communicatorLog("Communicator has been set up.");
	}

	/**
	 * Checks whether the port is opened
	 * @return boolean value true/false
	 */
	public boolean isPortOpened() {
    	return openport;
    }
    
    /**
     * Finds the ports that are currently connected.
     * @return List of ports as string.
     */
    private String[] GetAvailablePorts() {
    	SerialPort[] s = SerialPort.getCommPorts();
    	String[] portsAsString = new String[s.length];
    	for(int i = 0; i < s.length; i++) {
    		portsAsString[i] = s[i].getSystemPortName();
    	}
		return portsAsString;
    }
    
    /**
     * Sets the port for Communicator, with what it will communicate with Arduino.
     * @param portName Name of the port("COM30", "COM19" etc.).
     * @throws PortUnreachableException when port is unreachable.
     */
	private void SetPort(String portName) throws PortUnreachableException {
    	Logger.communicatorLog("Setting port :" + portName);
    	SerialPort[] s = SerialPort.getCommPorts();
        for(SerialPort obj : s) {
            if(obj.getSystemPortName().equals(portName)) {
                port = obj;
            }
        }
        if(port == null) {
        	Logger.communicatorLog("Failed to set port :" + portName + ". Current port might be occupied or it doesn't exist.");
        	throw new PortUnreachableException("Couldn't connect with given port. Current port might be occupied or it doesn't exist.");
        }
        Logger.communicatorLog("Port has been changed to :" + port.getSystemPortName());
    }

	/**
     * Method, that will start listening the incoming messages.
     */
    @Override
	public void run() {
		Logger.communicatorLog("Thread has been started.");
		while(activated) {
			if(openport) {
				checkStream();
			}else {
				connectWithArduino();
			}
			threadSleep(1);
        }
	}
    /**
     * Method, that checks, if stream has received any messages from arduino.
     */
    private void checkStream() {
    	if(port.bytesAvailable() == -1) {
    		openport = false;
    		closeConnections();
    		return;
    	}
		char readedChar = '\0';
		try {
			if(stream.available() == 0) {
				return;
			} else {
				readedChar = (char) stream.read();
			}
		} catch (IOException e) {
			Logger.communicatorLog("Something interrupted the stream: " + e);
			openport = false;
			closeConnections();
    		return;
		}
		switch(comState) {
		case Idle:
			// If the character that listener has received is 'v'
			if(readedChar == 'v') {
				comState = currentState.Sensor;
			} else if(readedChar == 'b') {
				comState = currentState.Button;
			}
			break;
		case Sensor:
			receivedMessage += readedChar;
			if(receivedMessage.length() == 2) {
				comState = currentState.SensorResponse;
			}
			break;
		case Button:
			receivedMessage += readedChar;
			comState = currentState.SensorResponse;
		case SensorResponse:
			comState = currentState.Idle;
			int receivedAsInt = Integer.parseInt(receivedMessage);
			receivedMessage += ":" + readedChar;
			//System.out.println("Hasnext: "+hasNext());
			if(!receivedMessage.equals(lastMessage) || receivedAsInt < 5) {
				IncomingSensorBuffer.add(new Sensor(receivedAsInt,readedChar != '1'));
				Logger.sensorsLog(new Sensor(receivedAsInt,readedChar != '1'));
				lastMessage = receivedMessage;
			}
			receivedMessage = "";
			break;
		}
    }
    /**
     * Method that checks through all of the ports connected to computer and checks if it is arduino or not.
     */
    private void connectWithArduino() {
    	isChecking = true;
    	for(SerialPort s :SerialPort.getCommPorts()) {
    		boolean portOpened = openPort(s);
    		if(!portOpened) {
    			threadSleep(50);
    			continue;
    		}
			InputStream instream = setinputStream(s);
    		if(instream == null) {
    			threadSleep(50);
    			s.closePort();
    			continue;
    		}
    		boolean validated = validate(s, instream);
    		if(validated) {
    			Logger.communicatorLog("Arduino device validated");
    			this.port = s;
    			this.stream = instream;
    			openport = true;
    			isChecking = false;
    			break;
    		}else {
    			Logger.communicatorLog("Arduino device is not validated");
    			s.closePort();
    			try {
					instream.close();
				} catch (IOException e) {
					
				}
    			
    		}
    	}
    	isChecking = false;
    }
    /**
     * Puts thread to sleep mode
     * @param millis milliseconds, that thread will be in sleep
     */
    private void threadSleep(long millis) {
    	try {
			sleep(millis);
		} catch (InterruptedException e) {
		}
    }
    /**
     * Opens the port for connecting with arduino.
     * @throws PortUnreachableException when port is unreachable.
     */
	private boolean openPort(SerialPort s){
    	Logger.communicatorLog("Trying to connect with port :" + s.getSystemPortName());
    	boolean portOpened = s.openPort();
    	if(!portOpened) {
    		Logger.communicatorLog("Couldn't open the port " + s.getSystemPortName());
    		return portOpened;
    	}
    	Logger.communicatorLog("Connected with port :" + s.getSystemPortName());
    	s.setBaudRate(9600);
        s.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER | SerialPort.TIMEOUT_WRITE_SEMI_BLOCKING, 0,0);
        s.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);
        return portOpened;
	}
	/**
     * Sets up the communication stream between arduino and PC.
     * @param port Port, from where the stream is being readed.
     * @throws PortUnreachableException when port is unreachable.
     */
    private InputStream setinputStream(SerialPort port){
    	Logger.communicatorLog("Trying to set up stream.");
    	InputStream instream = null;
    	try {
    		instream = port.getInputStream();
    		instream.available();
			Logger.communicatorLog("Stream has been set up.");
    	} catch(NullPointerException | IOException e) {
    		instream = null;
    		Logger.communicatorLog("Stream was found, but it was close right after.");
    	}
    	return instream;
    }
    /**
     * Validates, if the given port is arduino device
     * @param sPort Port, that will be checked
     * @param io Iostream, that is connected with given port
     * @return Returns true, if validation was successful, false otherwise.
     */
    private boolean validate(SerialPort sPort, InputStream io) {
    	try {
			sendMessage(sPort, 'c');
		} catch (IOException e) {
			return false;
		}
    	threadSleep(1000);
		try {
			if(io.available() > 0) {
				if((char) io.read() == 'c') {
					return true;
				}
			}
		} catch (IOException e) {
			Logger.communicatorLog("Something interrupted the stream: " + e);
		}
		return false;
    	
    }
    private void closeConnections() {
    	Logger.communicatorLog("Closed connection");
    	if(port != null) {
    		port.closePort();
    	}
    	try {
    		if(stream != null) {
    			stream.close();
    		}
		} catch (IOException e) {
			
		}
    }
    public void close() {
    	closeConnections();
    	activated = false;
    }
    private void sendMessage(SerialPort sPort, char message) throws IOException {
    	OutputStream out  = sPort.getOutputStream();
        out.write(message);
        out.flush();
        out.close();
    }

    public InputStream getStream() { return this.stream; }

    public SerialPort getPort() {
    	return this.port;
    }

    private void sendMessage(char message) throws IOException {
    	OutputStream out  = port.getOutputStream();
        out.write(message);
        out.flush();
        out.close();
    }

	public void getAllSensors() throws IOException {
		Logger.communicatorLog("Getting info about sensors states.");
		sendMessage('a');
    }

    /**
     * Checks if there is any sensor info in buffer
     * @return True, if info about sensor has arrived, false otherwise.
     */
    public boolean hasNext() {
    	return !IncomingSensorBuffer.isEmpty();
    }

    /**
     * Checks if there if (another) sensor
     * @return Returns sensor from buffer. Returns NULL if none.
     */
    public Sensor nextSensor() {
    	if(hasNext()) {
    		return IncomingSensorBuffer.poll();
    	}
    	return null;
    }
}
