package lm_main;

public enum AudienceViewState {
	PLAYER_VIEW,
	LIVE_VIDEO_VIEW,
	PROMO_VIDEO_VIEW,
	HOF_VIEW
}
