package lm_main;

import lm_arduino.Sensor;

/**
 * @usage Laser Maze Controller logging systems.
 * @author Team Laser Maze
 */
public class Logger {

	public static void communicatorLog(String message) {
		System.out.println("[COM] " + message);
	}
	
	public static void sensorsLog(Sensor sen) {
		System.out.println("[SENSOR] Sensor: " + sen.getSensorID()+ " [State: "+sen.isSensorActive() + "]");
	}
	
	public static void PCLogicLog(String message) {
		System.out.println("[PC LOGIC] " + message);
	}
	
	public static void soundLog(String message) {
		System.out.println("[SOUND] " + message);
	}
	
	public static void webCameraLog(String message) {
		System.out.println("[WEB CAM] " + message);
	}
	
	public static void UIlog(String message) {
		System.out.println("[UI] " + message);
	}
	
	public static void dbLog(String message) {
		System.out.println("[DB] " + message);
	}
}
