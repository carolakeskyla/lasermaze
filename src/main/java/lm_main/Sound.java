package lm_main;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

import javax.sound.sampled.*;

public class Sound {

	private static boolean playAmbientSound = true;
	private static boolean playOtherSounds = true;
	private static Sound sound;
	private HashMap<Sounds, Clip> soundFiles;
	private HashMap<String, String> sound2path;
	private final static String propertyFilePath = System.getProperty("user.dir") + File.separator + "sounds.property";

	private static boolean isPlayAmbientSound() {
		return playAmbientSound;
	}

	public static void setPlayAmbientSound(boolean playAmbientSound) {
		Sound.playAmbientSound = playAmbientSound;
	}

	private static boolean isPlayOtherSounds() {
		return playOtherSounds;
	}

	public static void setPlayOtherSounds(boolean playOtherSounds) {
		Sound.playOtherSounds = playOtherSounds;
	}

	public enum Sounds { activated, beep, damage, deactivated, greet, klaxon1, reset, success, voice_on, ambient }

	public static void main(String[] args) {
	    try(Scanner s = new Scanner(System.in)) {
		    int last = 0;
		    float currentVol = 1;
		    while(true) {
		    	if(s.hasNext()) {
		    		String val = s.next();
		    		switch(val) {
		    		case "q":
		    			return;
		    		case "p":
		    			Sound.playSound(Sounds.values()[last], false);
		    			break;
		    		case "o":
		    			Sound.changeVolume(Sounds.values()[last], currentVol -= 0.1);
		    			break;
		    		case "i":
		    			if(last >= Sounds.values().length-1) {
	    					last = 0;
		    			} else {
		    				last++;
		    			}
		    			break;
		    		case "u":
		    			Sound.playSound(Sounds.deactivated,false);
		    			break;
		    		}
		    	}
		    }
	    }
	}

	/**
	 * Function, that initializes sound singleton and adds sounds to it.
	 */
	private static void initialize() {
		sound = new Sound();
		sound.sound2path = new HashMap<String, String>();
		sound.soundFiles = new HashMap<Sounds, Clip>();
		sound.readSoundsPaths(propertyFilePath);
		sound.addSound(Sounds.activated, "activated");
		sound.addSound(Sounds.beep, "beep");
		sound.addSound(Sounds.damage, "damage");
		sound.addSound(Sounds.deactivated, "deactivated");
		sound.addSound(Sounds.greet, "greet");
		sound.addSound(Sounds.klaxon1, "klaxon1");
		sound.addSound(Sounds.reset, "reset");
		sound.addSound(Sounds.success, "success");
		sound.addSound(Sounds.voice_on, "voice_on");
		sound.addSound(Sounds.ambient, "ambient"); // Music from https://www.youtube.com/watch?v=ExGZ3VrgU6k
	}
	
	/**
	 * @usage Reads all sounds path.
	 * @param path File path of propery file which holds all sound file paths
	 */
	private void readSoundsPaths(String path) {
		try(Scanner sc = new Scanner(new File(path))) {
			while(sc.hasNext())
			{
				String[] parts = sc.next().split("<>");
				String firstPath = "";
				
				firstPath = System.getProperty("user.dir") + File.separator + "Sounds" + File.separator + parts[0] + ".wav";
				
				sound2path.put(parts[0], (!parts[1].equals("-") ? parts[1] : firstPath));
			}
		}
		catch(Exception e) {
			Logger.soundLog("Failed to load sounds.");
		}
	}
	
	/**
	 * @usage Change Specific sound path.
	 * @param sound - Sound name whose path to change.
	 * @param newPath - New path for sound.
	 */
	public static void changeSoundPath(String soundStr, String newPath) {
		if(!newPath.contains(".wav")) return;
		sound.sound2path.put(soundStr, newPath);
		
		for(Sounds s: sound.soundFiles.keySet()) {
			if(s.toString().equals(soundStr)) {
				File soundFile = new File(newPath);
				try {
					AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
					Clip clip = AudioSystem.getClip();
					clip.open(audioIn);
					sound.soundFiles.put(s, clip);
					saveNewSettingsToPropertyFile();
				}
				catch(Exception e) {
					Logger.soundLog("Couldn't change sound file.");
				}
				return;
			}
		}
	}
	
	/**
	 * @usage Save new settings to property file.
	 */
	private static void saveNewSettingsToPropertyFile() {
		try(PrintWriter writer = new PrintWriter(propertyFilePath, "UTF-8")) {
			for(String s: sound.sound2path.keySet()) {
				writer.println(s + "<>" + sound.sound2path.get(s));
			}
		}
		catch(Exception e) {}
	}

	/**
	 * Method, that adds sound to sound list.
	 * @param sound sound type.
	 * @param name name of the sound file.
	 */
	private void addSound(Sounds sound, String name) {
		try {
			File soundFile = new File(sound2path.get(name));
          	AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);      
          	Clip clip = AudioSystem.getClip();
          	clip.open(audioIn);
          	soundFiles.put(sound, clip);
          	Logger.soundLog("Added " + name + " to program.");
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
	        e.printStackTrace();
	    }
	}

	/**
	 * Method currently only used for test
	 * @param soundef sound effect from Sounds enum
	 * @return true/false whether a sound is playing
	 */
	public static boolean isPlaying(Sounds soundef) {
		if(sound == null) {
		   	initialize();
		}
		if(sound.soundFiles.containsKey(soundef)) {
			return sound.soundFiles.get(soundef).isRunning();
		}
		return false;
	}

	/**
	 * Function, that plays sounds
	 * @param playSound sound effect, that will be played. None, if the sound hasn't been read into the program.
	 */
	public static void playSound(Sounds playSound, boolean loop) {
		if(sound == null) {
		   initialize();
		}
		if(sound.soundFiles.containsKey(playSound)) {
			if(playSound == Sounds.ambient && isPlayAmbientSound() || playSound != Sounds.ambient && isPlayOtherSounds()) {
				Clip c = sound.soundFiles.get(playSound);
				c.stop();
				c.setFramePosition(0);
				c.start();
				if(loop) {
					c.loop(Clip.LOOP_CONTINUOUSLY);
				}
				Logger.soundLog("Playing sound "+ playSound.toString() + ".wav");
			}
		} else {
			Logger.soundLog("Trying to play sound " + playSound.toString() + ".wav , that isn´t in the Sounds folder. Maybe the file is named incorrectly?");
		}
	}

	/**
	 * Function, that stops sounds
	 * @param playSound sound effect, that will be stopped. None, if the sounds hasn't been read into the program.
	 */
	public static void stopSound(Sounds playSound) {
		if(sound == null) {
		   initialize();
		}
		if(sound.soundFiles.containsKey(playSound)) {
			Clip c = sound.soundFiles.get(playSound);
			c.stop();
		} else {
			Logger.soundLog("Trying to play music " + playSound.toString() + ".wav , that isn´t in the Sounds folder. Maybe the file is named incorrectly?");
		}
	}

	/**
	 * Method that changes the volume of given sound
	 * @param s sound effect, that will be changed
	 * @param val Value, that sound will have. It needs to be between [0, 1].
	 */
	public static void changeVolume(Sounds s, float val) {
		if(sound == null) {
			   initialize();
		}
		if(sound.soundFiles.get(s).isControlSupported(FloatControl.Type.MASTER_GAIN)) {
			FloatControl gainControl = (FloatControl) sound.soundFiles.get(s).getControl(FloatControl.Type.MASTER_GAIN);
			BooleanControl bc = (BooleanControl) sound.soundFiles.get(s).getControl(BooleanControl.Type.MUTE);
			bc.setValue(false);
			if(val >= 1) {
				gainControl.setValue(gainControl.getMaximum());
			} else if(val <= 0) {
				bc.setValue(true);
				gainControl.setValue(gainControl.getMinimum());
			} else {
				float dB = (float) (Math.log(val) / Math.log(10.0) * 20.0);
				gainControl.setValue(dB);
			}
		}
	}

	/**
	 * Changes the overall volume of all sounds to given value in Game Master view's slider
	 * @param val volume value
	 */
	public static void changeOtherVolumes(float val) {
		changeVolume(Sounds.activated, val);
		changeVolume(Sounds.beep, val);
		changeVolume(Sounds.damage, val);
		changeVolume(Sounds.deactivated, val);
		changeVolume(Sounds.greet, val);
		changeVolume(Sounds.klaxon1, val);
		changeVolume(Sounds.reset, val);
		changeVolume(Sounds.success, val);
		changeVolume(Sounds.voice_on, val);
	}
}
