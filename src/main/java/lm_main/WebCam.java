package lm_main;

import javafx.scene.image.Image;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;
import static org.opencv.videoio.Videoio.CV_CAP_PROP_FPS;

public class WebCam {
    
	private VideoCapture videoCapture;

    WebCam() {
    	initCamera();
    }

    /**
     * @usage Initialises the video capture for the current camera.
     */
    private void initCamera() {
        setLibraryPath();
        
        Globals.loadWebCamId();
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        //System.out.println(Paths.get(".").toAbsolutePath().normalize().toString());
        //System.loadLibrary("./libs/x64/opencv_java343.dll");
        videoCapture = new VideoCapture(Globals.webCamId);

        if(!videoCapture.isOpened())
            videoCapture.open(Globals.webCamId);
        //Setting of the frame size and framerate
        //for some reason on predefined width and hight, the webcam framerate drops, automatic is better.
        /*videoCapture.set(CV_CAP_PROP_FRAME_WIDTH,1280);
        videoCapture.set(CV_CAP_PROP_FRAME_HEIGHT,720);*/
        //videoCapture.set(CV_CAP_PROP_FPS,60);
        Logger.webCameraLog("Camera initialized");
    }
    /**
     *
     * @return video image in javafx format
     */
    public Image getImage() {
        Mat mat = new Mat();
        videoCapture.read(mat);
        MatOfByte buffer = new MatOfByte();
        Imgcodecs.imencode(".png", mat, buffer);
        return new Image(new ByteArrayInputStream(buffer.toArray()));
    }

    /**
     * @usage Sets up the path to library the openCV is using.
     */
    private static void setLibraryPath() {
        try {
            System.setProperty("java.library.path", "libs/x64");
            Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

    /**
     * @usage Closes the webcamera
     */
    public void close() {
    	videoCapture.release();
    }
}