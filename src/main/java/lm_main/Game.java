package lm_main;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import lm_arduino.Sensor;

/**
 * @usage Game data object.
 * @author Team Laser Maze
 */
@Entity
@Table(name = "Game")
public class Game {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "date")
	@Type(type="timestamp")
	private java.sql.Timestamp date;
	
	@Column(name = "maxMistakes")
	private int maxMistakes;
	
	@Column(name = "playTime")
	private long playTime;
	
	@Column(name = "mistakes")
	private long mistakes;
	
	@Column(name = "penaltyTime")
	private long penaltyTime;
	
	@Column(name = "sensors")
	private int sensors;
		
	//pauses and resumes
	@Transient
	private long PauseStart;

	//mistakes
	@Transient
	private GameState currentState;

	//times
	@Transient
	private Map<Long, Integer> currentMistakes;
	
	@Transient
	private long startTime;
	
	@Transient
	private long endTime;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<Sensor> allSensors;
	
	//game states
	private enum GameState{Running, Paused, Stopped};
	
	public Game () {}

	/**
	 * Constructor, that takes player name, max mistakes that can be made
	 * in game and maximum time that game can last.
	 * @param name name
	 * @param maxMistakes maximum mistakes
	 * @param penaltyTime - time added to time when mistake is made
	 */
	public Game(String name, Integer maxMistakes, float penaltyTime) {
		date = new java.sql.Timestamp(new Date().getTime());
		this.name = name;
		this.maxMistakes = maxMistakes;
		this.penaltyTime = (long) (penaltyTime*1000);
		currentState = GameState.Running;
		this.startTime = System.currentTimeMillis();
		currentMistakes = new HashMap<>();
		this.playTime = 0;
		this.mistakes = 0;
		this.sensors = 29;
	}

	/**
	 * Method that reads in the mistake, that was made.
	 * @param sensor Sensor ID that was interrupted
	 */
	public void mistake(int sensor) {
		currentMistakes.put(System.currentTimeMillis() - startTime, sensor);
		if(maxMistakesPassed()){
			Sound.playSound(Sound.Sounds.reset, false);
			endGame();
		}
	}

	/**
	 * Returns the number of mistake made by given sensor.
	 * @param sensor SensorID
	 * @return mistakes made by given sensor.
	 */
	public int getMistakesMade(int sensor) {
		int mistakes = 0;
		for (Integer item : currentMistakes.values()) {
			if (item == sensor) {
				mistakes++;
			}
		}
		return mistakes;
	}

	/**
	 * Method that checks, if maximum number of mistakes has been made.
	 * @return True, if maximum number of mistakes has been made, False otherwise.
	 */
	private boolean maxMistakesPassed() {
		return maxMistakes <= currentMistakes.size();
	}

	public String getPlayerName(){
	    return name;
	}

	public int getTotalMistakesMade(){ return currentMistakes.size(); }

	public long getFinalTime(){ return endTime;}

	/**
	 * returns the game status (running,paused,stopped)
	 * @return current state of the game
	 */
	public boolean isRunning(){
		return currentState == GameState.Running;
	}

	public boolean isPaused() {
		return currentState == GameState.Paused;
	}

	private GameState getGameStatus(){ return currentState; }

	void pauseGame() {
		currentState = GameState.Paused;
		PauseStart = System.currentTimeMillis();
	}

	void resumeGame() {
		currentState = GameState.Running;
		startTime += System.currentTimeMillis() - PauseStart;
	}

	/**
	 * Method,that returns taken time
	 * @return total time in milliSeconds
	 */
	public long timeTaken() {
		if(getGameStatus() == GameState.Running) {
			return (System.currentTimeMillis() - startTime + currentMistakes.size() * penaltyTime);
		} else if(getGameStatus() == GameState.Paused) {
			return PauseStart - startTime + currentMistakes.size() * penaltyTime;
		} else if(getGameStatus() == GameState.Stopped){
			return endTime;
		} else {
			return 0;
		}
	}
	
	/**
	 * Method, that is called, if end game button is pressed or if maxMistakes exceeded
	 */
	void endGame() {
		createSensorList();
		endTime = timeTaken();
		playTime = timeTaken();
		mistakes = getTotalMistakesMade();
		currentState = GameState.Stopped;
	}
	
	private void createSensorList()
	{
		allSensors = new ArrayList<Sensor>();
		
		for(int i = 0; i < 29; i++)
		{
			allSensors.add(new Sensor(i + 22, getMistakesMade(i + 22)));
		}
	}

	public void setPenaltyTime(int penaltyTime) {
		this.penaltyTime = penaltyTime;
	}
	
	public int getMaxMistakes() {
		return this.maxMistakes;
	}
	
	public int getSensors() {
		return this.sensors;
	}
	
	public long getPenaltyTime() {
		return this.penaltyTime;
	}
	
	public long getMistakes() {
		return this.mistakes;
	}
	
	public long getPlayTime() {
		return this.playTime;
	}

	public int getId() { return this.id; }
}