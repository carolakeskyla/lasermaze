package lm_main;

/**
 * @author Team Laser Maze
 * @date 04.10.2018
 * @usage
 * Laser maze project exception handling.
 */
public class LaserMazeException extends Exception {

	private static final long serialVersionUID = 1L;

	public LaserMazeException()
	{
		super();
	}
	
	public LaserMazeException(String message)
	{
		super(message);
	}
	
	public LaserMazeException(String message, Throwable cause)
	{
		super();
	}
}
