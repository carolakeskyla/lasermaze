package lm_main;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * @usage Controller for database managers.
 * @author Team Laser Maze
 */
public class DatabaseController {

	//private final EntityManagerFactory emf;
	//private final EntityManager em;
	
	public DatabaseController() {		
		//emf = Persistence.createEntityManagerFactory("lm");
        //em = emf.createEntityManager();
	}
	
	/**
	* @usage Database method. Closes em & emf.
	*/
	public void close() {
		//try {
		//	if(em.getTransaction().isActive()) {
		//		em.getTransaction().rollback();
		//	}
		//}
		//catch(Exception e) {}
        //em.close();
        //emf.close();
    }
	
	/**
	* @usage Database method. Begins transaction.
	*/
	public void beginGameTransaction() {
		Logger.dbLog("Begin transaction.");
		//em.getTransaction().begin();
	}
	
	/**
	* @usage Database method. Rolls back transaction.
	*/
	public void rollBackTransaction() {
		Logger.dbLog("Roll back transaction.");
		//em.getTransaction().rollback();
	}
	
	/**
	* @usage Database method. Commits transaction.
	*/
	public void commitTransaction() {
		Logger.dbLog("Commit transaction.");
		//em.getTransaction().commit();
	}
	
	/**
	* @usage Database method. Persist transaction.
	*/
	public void saveGame(Game game) {
		//em.persist(game);
	}

	/**
	 * @usage Database method. Removes last game from the database
	 */
	public void deleteLastGame() {
		List<Game> games = getFromDatabase("FROM Game");
		if (games == null) {
			return;
		} if (games.isEmpty()) {
			return;
		}

		int lastGame = games.size();
		beginGameTransaction();
		try {
			//em.merge(em.find(Game.class, Globals.gameId));
			//Call out database update
			//em.remove(em.find(Game.class, Globals.gameId));
			commitTransaction();
			Logger.dbLog("Remove lastGame with id " + Globals.gameId);
			Globals.gameId = -1;
		} catch (Exception e){
			rollBackTransaction();
		}
	}

	/**
	 * Database method. Get data from database using SQL statement.
	 * @param command SQL statement
	 */
	public List<Game> getFromDatabase(String command) {
		//TypedQuery<Game> query = em.createQuery(command, Game.class);
		//return (query == null) ? new ArrayList<Game>() : query.getResultList();
		return new ArrayList<Game>();
	}
}