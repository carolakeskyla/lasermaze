package lm_main;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * @usage Global variables.
 * @author Team Laser Maze
 */
public class Globals {

	public static final int EndButton = 3;
	public static final int StartButton = 2;
	public static String name = "Player";
	public static int maxMistakes = 10;
	public static float penaltyTime = 2.0f;
	public static int gameId = -1;
	public static int webCamId = 0;
	
	private static final String webCamPropertyPath = System.getProperty("user.dir") + File.separator + "webcam.property";
	
	public static void loadWebCamId()
	{
		try(Scanner sk = new Scanner(new File(webCamPropertyPath))) {
			if(sk.hasNext()) {
				webCamId = sk.nextInt();
			}
		}
		catch(IOException e) {}
	}
}
