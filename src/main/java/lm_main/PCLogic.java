package lm_main;

import java.io.IOException;
import java.util.HashMap;
import java.util.Random;
import lm_application.UI;
import lm_arduino.*;
import lm_main.Sound.*;

public class PCLogic extends Thread {

	public float sensorDelay = 1000;
	private Communicator com;
	private WebCam cam;
	private UI ui;
	private Game currentGame;
	private Game lastGame;
	private HashMap<Sensor, Long> sensors;
	private DatabaseController databaseController;
	private boolean UIInfoSent = false;
	private boolean runPcLogic = true;
	
	public PCLogic(UI ui) {
		Logger.PCLogicLog("Initializing main class..");
		try {
			Runtime.getRuntime().exec("cmd /c start \"\" lm.bat");
		} catch (IOException e) {
			System.exit(1);
		}
		this.ui = ui;
		sensors = new HashMap<>();
		Sound.playSound(Sounds.voice_on, false);
		com = new Communicator();
		//cam = new WebCam();
		databaseController = new DatabaseController();
		this.start();
		Logger.PCLogicLog("Main class initialized.");
	}
	public void AddSensor(Sensor s){
		sensors.put(s, System.currentTimeMillis());
	}
	public void run() {
		while(runPcLogic) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(com.isPortOpened()) {
				if(UIInfoSent) {
					ui.arduinoDisconnected(false);
					UIInfoSent = false;
					Logger.PCLogicLog("Arduino has been connected.");
				}
				while(com.hasNext()) {
					Sensor s = com.nextSensor();
					System.out.println("Sensor: " + s);
					if(s.getSensorID() < 4) {
						handleButton(s);
					} else {
						handleSensor(s);
					}
				}
			} else {
				if(!UIInfoSent) {
					// NOTE: Modified on 19/11/2020 for SQ lab:
					// all 2 true -> false
					ui.arduinoDisconnected(false);
					UIInfoSent = false;
					//Logger.PCLogicLog("Arduino has been disconnected.");
				}
			}
			if(currentGame != null) {
				if(getGameStatus().equals("Stopped")) {
                    endGame();
                }
				ui.setTime();
			}

			//ui.sendCamInfo(cam.getImage());
		}
	}

    /**
     * Handles physical start and stop buttons
     * @param s sensor
     */
	private void handleButton(Sensor s) {
		if(currentGame!=null) {
			if(currentGame.isPaused() || currentGame.isRunning()) {
                if(s.getSensorID() == Globals.EndButton) {
                    endGame();
                    Sound.playSound(Sounds.success, false);
                    ui.gameStopped();
                }
            }
		} else {
			if(s.getSensorID() == Globals.StartButton) {
				startGame();
				ui.gameStarted();
			}
		}
	}

    /**
     * Closes everything when UI is closed.
     */
	public void close() {
		Logger.PCLogicLog("Main class has been closed.");
		com.close();
		databaseController.close();
		//cam.close();
		runPcLogic = false;
		System.exit(0);
	}

    /**
     * Handles and controls sensor states
     * @param s sensor
     */
	private void handleSensor(Sensor s) {
		Sensor keyFromMap = getSensorFromMap(s.getSensorID());
		int sensId = keyFromMap.getSensorID();

		//Game logic
		if(currentGame != null) {
			if(currentGame.isRunning()) {
				if(mistakeMade(keyFromMap)&&keyFromMap.isUsedInGame()) {
					Sound.playSound(Sounds.klaxon1, false);
					currentGame.mistake(keyFromMap.getSensorID());
				}
			} else if (!currentGame.isPaused()) {
				endGame();
			}
		}

		// Sensors logic
		keyFromMap.setSensorState(s.isSensorActive());
		sensors.put(keyFromMap, System.currentTimeMillis());
		if(currentGame == null) {
			if(lastGame == null) {
				ui.setSensor(sensId,keyFromMap.isSensorActive(), 0);
			}else {
				ui.setSensor(sensId,keyFromMap.isSensorActive(), lastGame.getMistakesMade(sensId));
			}
		} else {
			ui.setSensor(sensId,keyFromMap.isSensorActive(), currentGame.getMistakesMade(sensId));
		}
	}

    /**
     * Checks whether a mistake was real and not caused by the sensor being inactive.
     * @param sensor sensor which was interrupted.
     * @return true if a mistake was valid, false if mistake was not valid and the sensor is inactive.
     */
	public boolean mistakeMade(Sensor sensor) {
		long time = sensors.get(sensor);
		if(System.currentTimeMillis() >= time + (long)sensorDelay) {
			if(sensor.isSensorActive()) {
				return true;
			}
		}
		return false;
	}

    /**
     * Gets correct sensor
     * @param id id of sensor which Arduino sends
     * @return sensor
     */
	public Sensor getSensorFromMap(int id) {
		for(Sensor SensorFromKey : sensors.keySet()) {
			if(SensorFromKey.getSensorID() == id) {
				return SensorFromKey;
			}
		}
		Sensor keyFromMap = new Sensor(id, true);
		sensors.put(keyFromMap, (long)(System.currentTimeMillis() - sensorDelay - 1));
		return keyFromMap;
	}

    /**
     * Method for resetting all the mistakes and states.
     */
	public void makeEmptySensors() {
		for(int i = 22; i < 50; i++) {
            handleSensor(getSensorFromMap(i));
        }
	}

    /**
     * Method used for testing purposes
     */
	public void makeATestMistake() {
		Random rand = new Random();
		int id = rand.nextInt(29) + 22;
		handleSensor(getSensorFromMap(id));
	}

	/**
	 * Method used for testing purposes
	 */
	public void makeATestMistake(int idx) {
		handleSensor(getSensorFromMap(idx));
	}

	/**
	* Sensor id is given from UI as 1,2,3.., method makes it use Arduino's id's(1 = 22 etc.)
     * and activates (meaning the mistakes are registered by the sensor)/ deactivates the sensor.
     * @param id id of sensor.
     * @param usage true if sensor is ticked in Game Master view, false otherwise
     */
	public void setSensorInGameUsage(int id, boolean usage) {
		id += 21;
		try {
			Sensor temp = getSensorFromMap(id);
			Long time = sensors.get(temp);
			sensors.remove(temp);
			temp.setUseInGame(usage);
			sensors.put(temp, time);
		}
		catch (Exception e) {
			Logger.PCLogicLog("Sensor with an id : "+ id +" not found");
		}
	}

	public void setSensorDelay(float sensorDelay) {
		Logger.PCLogicLog("Sensor delay has been set to " + sensorDelay);
		this.sensorDelay = sensorDelay;
	}

	public void getSensorStatuses() throws IOException {
		com.getAllSensors();
	}

    /**
     * @usage Makes new game.
     */
	public void makeGame() {
		Logger.PCLogicLog("New instance of game has been created.");
		currentGame = new Game(Globals.name, Globals.maxMistakes, Globals.penaltyTime);
	}

	public long getGameTimeAsLong() {
		if(lastGame != null && currentGame == null) {
			return lastGame.timeTaken();
		}
		if(currentGame==null) {
            return 0;
        }
		return currentGame.timeTaken();
	}

	public String getGameTimeAsString() {
		return timeFormatter(getGameTimeAsLong());
	}

	public static void setPlayerName(String name) {
		Logger.PCLogicLog("Player name is set to "+ name);
		Globals.name = name;
	}

    /**
     * Sets max amount of mistakes allowed per game
     * @param mistakes Maximum amount of mistakes allowed entered in Game Master view
     */
	public static void setMaxMistakes(int mistakes) {
		Globals.maxMistakes = mistakes;
		Logger.PCLogicLog("Global max mistakes edited: " + mistakes);
	}

    /**
     * Sets penalty time for each mistake made
     * @param time Penalty time for each mistaked entered in Game Master view
     */
	public static void setPenaltyTime(float time) {
		Globals.penaltyTime = time;
		Logger.PCLogicLog("Global penalty time edited: " + time);
	}

	/**
	* Takes long millis as input and formats it
	* @return formatted string with min:sec:millis
	*/
	public String timeFormatter(long time) {
		long millis = time % 1000;
		long second = (time / 1000) % 60;
		long minute = (time / (1000 * 60)) % 60;
		return String.format("%02d:%02d.%d", minute, second, millis);
	}

    /**
     * Checks how many mistakes have been made
     * @return amount of total mistakes made
     */
	public int getMadeMistakes() {
		if(currentGame==null) {
            return 0;
        }
		return currentGame.getTotalMistakesMade();
	}

	public void startGame() {
		if(currentGame != null) {
			if (currentGame.isPaused()) {
				Sound.playSound(Sounds.activated, false);
				currentGame.resumeGame();
				Logger.PCLogicLog("Game is resumed.");
			}
		} else {
			makeGame();//In method makeGame() the game is also started
			Logger.PCLogicLog("Game is started.");
			Sound.playSound(Sounds.activated, false);
		}
	}

	public void pauseGame() {
		if(currentGame != null) {
			ui.gamePaused();
            if (!currentGame.isPaused()) {
                Sound.playSound(Sounds.deactivated, false);
                currentGame.pauseGame();
                Logger.PCLogicLog("Game is paused.");
            }
        }
	}

	public Game getLastGame() {
		return lastGame;
	}

	public void endGame() {

		if(currentGame != null) {
			Logger.PCLogicLog("Game is stopped.");
			ui.gameStopped();
			lastGame = currentGame;
			lastGame.endGame();
			currentGame = null;
			
			try {
				Sound.playSound(Sounds.success, false);
				databaseController.beginGameTransaction();
				databaseController.saveGame(lastGame);
				Globals.gameId = lastGame.getId();
				databaseController.commitTransaction();
				
			} catch (Exception e) {
				databaseController.rollBackTransaction();
				System.out.println(e.getMessage());
			}
			
			ui.setTime();
			ui.normalizeState();
		}
	}

    /**
     * Checks whether a game is running, paused or stopped
     * @returns current game status (running, paused or stopped)
     */
	public String getGameStatus() {
		if(currentGame != null) {
			if(currentGame.isRunning()) {
				return "Running";
			} else if(currentGame.isPaused()) {
				return "Paused";
			} else {
				return "Stopped";
			}
		} else {
			return "Stopped";
		}
	}
	
	public Game getCurrentGame() {
		return currentGame;
	}
	
	public DatabaseController getDbController() {
		return this.databaseController;
	}
}
