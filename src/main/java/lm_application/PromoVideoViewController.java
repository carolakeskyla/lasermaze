package lm_application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.media.MediaView;
import lm_main.Logger;

/**
 * @author Team Laser Maze
 * @usage Controls when a promo video is shown.
 * 1. This is the first view when starting the application.
 * 2. A promo video is shown every 10 seconds. After, the HoFViewController will be shown for 10 seconds.
 * 3. Pressing any key results in showing PlayerViewController.
 */
public class PromoVideoViewController implements Initializable {
	
    @FXML 
    private MediaView promoVideo;

    public PromoVideoViewController(LaserMazeUI laserMazeUI) {}

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Logger.UIlog("Promo video view initialized.");
        promoVideo.getMediaPlayer().setVolume(0);
    }
    
    /**
     * @usage Stops promo video.
     */
    public void stopPromoVideo() {
    	promoVideo.getMediaPlayer().stop();
    }
    
    /**
     * @usage Starts promo video.
     */
    public void startPromoVideo() {
    	promoVideo.getMediaPlayer().play();
    }
}
