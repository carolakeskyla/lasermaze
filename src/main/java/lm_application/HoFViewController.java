package lm_application;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import lm_main.Game;
import lm_main.Globals;
import lm_main.Logger;

/**
 * @usage JavaFX controller for the hall of fame view.
 * 1. This is the second view after starting the application (10 seconds after PromoVideoViewController && when no key has been pressed by player).
 * 2. If a game has ended the Hall of Fame will be shown (for 10 seconds). Continues with 1. point logic.
 * 3. The Hall of Fame shown every 10 seconds. After, the PromoVideoViewController will be shown for 10 seconds.
 * 4. Pressing any key results in showing PlayerViewController.
 * @author Team Laser Maze
 *
 */
public class HoFViewController implements Initializable {
	
	private LaserMazeUI laserMazeUI;

    @FXML
    private Label hofLabel;
    @FXML
    private GridPane hofGrid;
    @FXML
    private Label[][] tableValue = new Label[6][13];
    
    private String timeframe;
    
    private boolean inTopTen;

    public HoFViewController(LaserMazeUI laserMazeUI) {
        this.laserMazeUI = laserMazeUI;
        this.timeframe = "thisWeek";
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Logger.communicatorLog("Hall of Fame view initialized.");
        
        inTopTen = false;

        changeTimeframe();

        //Fills grid with top 10 values
        fillGrid(getTopTen(timeframe));
    }

    /**
     * @usage Changes HoFView label based on user's selection. Possible input values: today, thisWeek, thisMonth
     */
    @FXML 
    private void changeTimeframe() {
        switch (this.timeframe) {
            case "today":
                hofLabel.setText("EDETABEL: TÄNA");
                break;
            case "thisWeek":
                hofLabel.setText("EDETABEL: SEE NÄDAL");
                break;
            case "thisMonth":
                hofLabel.setText("EDETABEL: SEE KUU");
                break;
            default:
                hofLabel.setText("EDETABEL: LÄBI AEGADE");
                break;
        }
    }
    
    /**
     * @usage Fills HoF grid with values from database.
     * @param topTen Values with to fill HoF grid.
     */
    private void fillGrid(String[][] topTen) {
        for (int i = 0; i < tableValue.length; i++) {       
        	for (int j = 1; j < tableValue[i].length; j++) {
                tableValue[i][j] = new Label();
                
                //Sets numbers to players according to their position eg. 1., 2., 3...
                if (i == 0) {
                	if(j != 11 && j != 12)
                		tableValue[i][j].setText(j + ". ");
                } else {
                	if(j != 11 && j != 12)
                		tableValue[i][j].setText(topTen[j - 1][i - 1]);
                }
                
                tableValue[i][j].getStyleClass().add("hof-table-value");
                hofGrid.add(tableValue[i][j], i, j);
            }
        }
    }
    
    /**
     * Gets current timestamp based on GM view choice
     * @param timeframe chosen timeframe String
     * @return timeframe
     */
    private java.sql.Timestamp getTimeStamp(String timeframe) {
        LocalDateTime begin = LocalDateTime.now();

        switch (timeframe) {
            case "today":
                begin = begin.minusHours(begin.getHour());
                begin = begin.minusMinutes(begin.getMinute());
                begin = begin.minusSeconds(begin.getSecond());
                break;
            case "thisWeek":
                begin = begin.minusWeeks(1);
                break;
            case "thisMonth":
                begin = begin.minusMonths(1);
                break;
            case "all":
                begin = LocalDateTime.now().minusYears(100);
                break;
            default:
                break;
        }

        java.sql.Timestamp beginTimeStamp = new java.sql.Timestamp(Date.from(begin.atZone(ZoneId.systemDefault()).toInstant()).getTime());
        return beginTimeStamp;
    }

    /**
     * @usage Gets top ten players from database from specific date.
     * @param timeframe Date from to get top ten players.
     * @return top ten of players.
     */
    public String[][] getTopTen(String timeframe) {
    	boolean localIsInTopTen = false;
    	
    	java.sql.Timestamp beginTimeStamp = getTimeStamp(timeframe);
    	
        List<Game> games = this.laserMazeUI.getPCLogic().getDbController().getFromDatabase("FROM Game WHERE date >= '" + beginTimeStamp.toString() + "' ORDER BY playTime ASC");
    	
    	String[][] topTen = new String[11][6];
    	
    	if(games == null) return topTen;
    	if(games.isEmpty()) return topTen;
    	
    	int iterationCount = (games.size() <= 10) ? games.size() : 10;
    	
    	for(int i = 0; i < iterationCount; i++) {
    		if(games.get(i).getId() == Globals.gameId) {
    			localIsInTopTen = true;
    			inTopTen = true;
    		}
    		
    		if(localIsInTopTen) {
    			topTen[i][0] = games.get(i).getPlayerName() + "<x9301top24>";
        		topTen[i][1] = games.get(i).getSensors() + "<x9301top24>";
        		topTen[i][2] = (games.get(i).getPenaltyTime() / 1000) + " s" + "<x9301top24>";
        		topTen[i][3] = games.get(i).getMistakes() + "<x9301top24>";
        		String playTime = laserMazeUI.getPCLogic().timeFormatter(games.get(i).getPlayTime());
        		topTen[i][4] = playTime.substring(0, playTime.length() - 2) + "<x9301top24>";
        		localIsInTopTen = false;
    		}
    		else {
    			topTen[i][0] = games.get(i).getPlayerName();
        		topTen[i][1] = games.get(i).getSensors() + "";
        		topTen[i][2] = (games.get(i).getPenaltyTime() / 1000) + " s";
        		topTen[i][3] = games.get(i).getMistakes() + "";
        		String playTime = laserMazeUI.getPCLogic().timeFormatter(games.get(i).getPlayTime());
        		topTen[i][4] = playTime.substring(0, playTime.length() - 2);
    		}
    	}
    	
        return topTen;
    }
    
    /**
    * @usage Returns current player's game in order to get its information.
    */
   private Game getCurrentPlayersGame() {
       List<Game> game = this.laserMazeUI.getPCLogic().getDbController().getFromDatabase("FROM Game WHERE id = " + Globals.gameId);

       if(game == null) {
           return null;
       }
       if (game.isEmpty()) {
           return null;
       }

       return game.get(0);
   }
   
   /**
    * @usage Returns the rank of the current player.
    */
   private int getRank(String timeframe) {

       int rank = 0;

       java.sql.Timestamp beginTimeStamp = getTimeStamp(timeframe);

       List<Game> games = this.laserMazeUI.getPCLogic().getDbController().getFromDatabase("FROM Game WHERE date >= '" + beginTimeStamp.toString() + "' ORDER BY playTime ASC");

       if(games == null) {
           return rank;
       } if (games.isEmpty()) {
           return rank;
       }

       for(Game g : games) {
    	   rank += 1;
    	   if(g.getId() == Globals.gameId) {
    		   return rank;
    	   }
       }
       
       return rank;
   }
    
    /**
     * @usage Updates scoreboard values.
     * @param topTen Values to add to scoreboard.
     */
    private void updateGrid(String[][] topTen) {
		Platform.runLater(new Runnable() {
			@Override public void run() {
				for (int i = 0; i < tableValue.length; i++) { 
		    		for (int j = 1; j < tableValue[i].length - 2; j++) { 
		    			//Sets numbers to players according to their position eg. 1., 2., 3... 
		    			if (i == 0) { 
		    				tableValue[i][j].setText(j + ". "); 
		    			} else {
		    				tableValue[i][j].setText("");
		    				if(topTen[j - 1][i - 1] != null) {
		    					if(topTen[j - 1][i - 1].contains("<x9301top24>")) {
		    						tableValue[i][j].setText(topTen[j - 1][i - 1].replaceAll("<x9301top24>", ""));
		    						tableValue[i][j].setStyle("-fx-text-fill: #F9CF5C");
		    					}
		    					else {
		    						tableValue[i][j].setText(topTen[j - 1][i - 1]);
		    						tableValue[i][j].setStyle("-fx-text-fill: #EF8A7F");
		    					}
		    				}
		    			}
		    		}
		    	}
				
				if(Globals.gameId < 0) {
					inTopTen = true;
				}
					
				if(!inTopTen) {
					int rownum = 12;
					Game g = getCurrentPlayersGame();
					tableValue[0][11].setText("...");
	                tableValue[0][12].setText(Integer.toString(getRank(timeframe)) + ". ");
	                
	                List<String> infoForGame = new ArrayList<String>();
	                infoForGame.add(g.getPlayerName());
	                infoForGame.add(g.getSensors() + "");
	                infoForGame.add(g.getPenaltyTime() / 1000 + " s");
	                infoForGame.add(g.getMistakes() + "");
	                String playTime = laserMazeUI.getPCLogic().timeFormatter(g.getPlayTime());
	                infoForGame.add(playTime.substring(0, playTime.length() - 2));
	                
			        for (int i = 0; i < tableValue.length; i++)
			            if (i != 0) {
			                tableValue[i][rownum].setText(infoForGame.get(i - 1));
			                tableValue[i][rownum].setStyle("-fx-text-fill: #F9CF5C");
			            }
					inTopTen = false;
				}
				else {
			        for (int i = 0; i < tableValue.length; i++) {
			        	tableValue[i][11].setText("");
			        	tableValue[i][12].setText("");
			        }
				}
				inTopTen = false;
			}
		});
    }
    
    /**
     * @usage Update grid with top 10 values.
     */
    public void update() {
    	//Fills grid with top 10 values
        Platform.runLater(new Runnable() {
			@Override public void run() {
				updateGrid(getTopTen(timeframe));
			}
		});
    }
    
    /**
     * @usage Updates the time frame value of this instance. 
     * @param newTimeframe
     */
    public void updateTimeframe(String newTimeframe) {
    	Platform.runLater(new Runnable() {
			@Override public void run() {
		    	timeframe = newTimeframe;
		    	changeTimeframe();
		    	update();
			}
		});
    }
}