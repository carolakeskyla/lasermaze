package lm_application;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import lm_main.Logger;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @usage JavaFX controller for audience view. Audience view operates on a third screen.
 * @author Team Laser Maze
 * @usage Controller for live video view.
 */
public class LiveViewController implements Initializable {
	
	private boolean viewShown = false;

	@FXML 
	private VBox audienceViewMain;
	@FXML 
	private ImageView cameraView;
	@FXML 
	private Label textPlayer;
	@FXML 
	private Label textTime;
	@FXML 
	private Label textMistakes;

	public LiveViewController(LaserMazeUI laserMazeUI) {}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Logger.UIlog("Live view initialized.");
		textTime.setText("Mäng ei käi.");
	}

	/**
	 * @usage Set image for camera.
	 * @param img Image to set.
	 */
	public void setCameraImage(Image img) {
		if(!viewShown) return;
		
		Platform.runLater(new Runnable() {
			@Override 
			public void run() { cameraView.setImage(img); }
		});
	}
	
	/**
	 * @usage Set current player name.
	 * @param playerName Player name to set.
	 */
	public void setPlayerName(String playerName){

		Platform.runLater(new Runnable() {
			@Override 
			public void run() { textPlayer.setText("Mängija: " + playerName); }
		});
	}
	
	/**
	 * @usage Set current time.
	 * @param time Time to set
	 */
	public void setTextTime(String time) {
		Platform.runLater(new Runnable() {
			@Override 
			public void run() { textTime.setText("Aega kulunud: " + time); }
		});

	}
	
	/**
	 * @usage Set current mistakes.
	 * @param count Number of mistakes
	 */
	public void setTextMistakes(int count) {
		Platform.runLater(new Runnable() {
			@Override 
			public void run() {textMistakes.setText("Vigu: " + count);}
		});
	}
	
	/**
	 * @usage Helper method to reset all mistakes.
	 */
	public void resetMistakes() {
		setTextMistakes(0);
	}
	
	/**
	 * @usage Disable live view.
	 */
	public void disable() {
		viewShown = false;
	}
	
	/**
	 * @usage Activate live view.
	 */
	public void activate() {
		viewShown = true;
	}
	
	/**
	 * @usage Clear textTime text.
	 */
	public void clearTextTime()
	{
		Platform.runLater(new Runnable() {
			@Override 
			public void run() { textTime.setText("Mäng ei käi."); }
		});
	}
}
