package lm_application;

import javafx.scene.image.Image;

/**
 * @usage Interface for UI classes.
 * @author Team Laser Maze
 */
public interface UI {
	public void arduinoDisconnected(boolean value);
	public void setTime();
	public void setSensor(int sensorID, boolean currentVal, int currentMistakes);
	public void sendCamInfo(Image img);
	public void normalizeState();
	public void gameStopped();
	public void gamePaused();
	public void gameStarted();
}
