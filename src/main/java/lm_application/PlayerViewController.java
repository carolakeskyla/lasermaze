package lm_application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lm_main.*;

/**
 * @author Team Laser Maze
 * @date 03.10.2018
 * @usage Java FX controller for player view.
 */
public class PlayerViewController implements Initializable {
	
	@FXML 
	private TextField playerNameField;
	
	private LaserMazeUI laserMazeUI;
	
	public PlayerViewController(LaserMazeUI laserMazeUI) {
		this.laserMazeUI = laserMazeUI;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Logger.UIlog("Player view initialized.");
	}
	
	/**
	 * @usage Player presses enter then his/her name is saved to current game.
	 * @param event
	 */
	@FXML
	private void handleOnKeyPressed(KeyEvent event) {
		if(event.getCode().equals(KeyCode.ENTER)) {
			Logger.UIlog("Player " + playerNameField.getText() + " entered his/her name.");
			laserMazeUI.setPlayersName(playerNameField.getText());
			laserMazeUI.setScreen("liveView");
		}
	}
	
	/**
	 * @usage Clears player name field.
	 */
	public void clearName() {
		playerNameField.clear();
	}
}
