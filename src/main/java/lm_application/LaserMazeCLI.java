package lm_application;

import java.io.IOException;
import java.util.Scanner;
import javafx.scene.image.Image;
import lm_main.PCLogic;

/**
 * @author Daniel Nael
 * @date 14.10.2018
 * @usage Laser Maze main CLI class ----> DO NOT PEER REVIEW THIS! <------ ----> DO NOT PEER REVIEW THIS! <------ ----> DO NOT PEER REVIEW THIS! <------
 */
public class LaserMazeCLI implements UI
{
	private static PCLogic pcLogic;
	private boolean running = true;
	private boolean taskDone = true;
	
	public static void main(String[] args) throws IOException
	{
		LaserMazeCLI lmConsole = new LaserMazeCLI();
		pcLogic = new PCLogic(lmConsole);
		lmConsole.run();
		
	}
	
	private void run() throws IOException
	{
		welcomeScreen();
		infoScreen();
		
		String task;
		
		Scanner sk = new Scanner(System.in);
				
		while(running)
		{
			if (taskDone)
			{
				task = sk.nextLine();
				System.out.println("Processing: '" + task + "'");
				taskDone = false;
				if(task != null) processTask(task);
			}
		}
		
		sk.close();
	}
	
	private void welcomeScreen()
	{
		System.out.println("SOFTWARE CONTROLLER FOR LASER MAZE");
		System.out.println("Carola Kesküla, Raigo Kõvask, Ott Saar, Daniel Nael");
		System.out.println("-----------------------------------------------------");
	}
	
	private void infoScreen()
	{
		System.out.println("List of tasks LaserMazeCLI supports:");
		System.out.println("q\t\tExit");
		System.out.println("h\t\tShow this help");
		System.out.println("p NAME\t\tEnter player's NAME");
		System.out.println("s\t\tStart the game");
		System.out.println("e\t\tEnd the game");
		System.out.println("p\t\tPause the game");
		System.out.println("t\t\tSee game time if game is started.");
		System.out.println("a PENALTY\t\tChange penalty time with PENALTY");
		System.out.println("f MAX_ALLOWED_MISTAKES\t\t Change max allowed mistakes with MAX_ALLOWED_MISTAKES");
		System.out.println("n\t\t Get current player's name");
		System.out.println("m\t\t Get current max allowed mistakes");

	}
	
	private void processTask(String task)
	{
		String[] taskComponents = task.split(" ");

		switch(taskComponents.length)
		{
			case 1:
				taskLen1(taskComponents[0].charAt(0));
				break;
			case 2:
				taskLen2(taskComponents[0].charAt(0), taskComponents[1]);
				break;
			default:
				System.out.println("Task '" + task + "' doesn't exist.");
				break;
		}
	}
	
	private void taskLen1(char c)
	{
		switch(c)
		{
			case 'q':
				System.out.println("System exit.");
				running = false;
				taskDone = true;
				break;
			case 'h':
				infoScreen();
				taskDone = true;
				break;
			case 's':
				System.out.println("Creating game ...");
				pcLogic.makeGame();
				pcLogic.startGame();
				System.out.println("Game started.");
				taskDone = true;
				break;
			case 't':
				if(pcLogic.getGameStatus().equals("Running"))
					System.out.println(pcLogic.getGameTimeAsString());
				taskDone = true;
				break;
			case 'p':
				if(pcLogic.getGameStatus().equals("Running"))
				{
					pcLogic.pauseGame();
					System.out.println("Game paused. \nCurrent time: " + pcLogic.getGameTimeAsString());
				}
				else
				{
					System.out.println("Can't pause because game is not running.");
				}
				taskDone = true;
				break;
			case 'e':
				if(pcLogic.getGameStatus().equals("Running"))
				{
					pcLogic.endGame();
					System.out.println("Game stopped. \nFinal time: " + pcLogic.getGameTimeAsString());
				}
				else
				{
					System.out.println("Can't stop because game is not running.");
				}
				taskDone = true;
				break;
			case 'm':
				if(pcLogic.getGameStatus().equals("Running")) System.out.println("Current max mistake number = " + pcLogic.getCurrentGame().getMaxMistakes());
				taskDone = true;
				break;
			case 'n':
				if(pcLogic.getGameStatus().equals("Running")) System.out.println("Current player: " + pcLogic.getCurrentGame().getPlayerName());
				taskDone = true;
				break;
			default:
				taskDone = true;
				break;
		}
	}
	
	private void taskLen2(char c0, String c1)
	{
		if(c0 == 'a' && toFloat(c1) > 0)
		{
			if(pcLogic.getGameStatus().equals("Running")) System.out.println("WARNING! Current game penalty time won't be updated!\nGame is running.");
			PCLogic.setPenaltyTime(toFloat(c1));
		}
		else if(c0 == 'p' && c1 != null)
		{
			if(pcLogic.getGameStatus().equals("Running")) System.out.println("WARNING! Current game player name time won't be updated!\nGame is running.");
			PCLogic.setPlayerName(c1);
		}
		if(c0 == 'a' && toInt(c1) > 0)
		{
			if(pcLogic.getGameStatus().equals("Running")) System.out.println("WARNING! Current game max mistakes number won't be updated!\nGame is running.");
			PCLogic.setMaxMistakes(toInt(c1));
		}
		
		taskDone = true;
	}
	
	private float toFloat(String c1)
	{
		float f = -500;
		
		try
		{
			f = Float.parseFloat(c1);
		}
		catch(Exception e)
		{
			System.out.println(c1 + " ei ole arv.");
		}
		
		return f;
	}
	
	private int toInt(String c1)
	{
		int in = -500;
		
		try
		{
			in = Integer.parseInt(c1);
		}
		catch(Exception e)
		{
			System.out.println(c1 + " ei ole arv.");
		}
		
		return in;
	}
	
	@Override
	public void arduinoDisconnected(boolean value) {}

	@Override
	public void setTime() {}

	@Override
	public void setSensor(int sensorID, boolean currentVal, int currentMistakes) {}

	@Override
	public void sendCamInfo(Image img) {}

	@Override
	public void normalizeState() {}

	@Override
	public void gameStopped() {}

	@Override
	public void gamePaused() {}

	@Override
	public void gameStarted() {}
}