package lm_application;

import java.net.URL;
import java.util.Arrays;
import java.util.Objects;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import lm_main.*;
import lm_main.Sound.Sounds;

/**
 * @author Team Laser Maze
 * @usage Java FX controller for game master view.
 */

public class GameMasterViewController implements Initializable {
	
	private LaserMazeUI laserMazeUI;

	@FXML 
	private Button startButton;
	@FXML 
	private Button stopButton;
    @FXML 
    private Button pauseButton;
	@FXML 
	private Button resumeButton;
	@FXML 
	private Text gameState;
	@FXML 
	private Text playersName;
	@FXML 
	private Text penaltyTimeLabel;
	@FXML 
	private Text maxMistakesLabel;
	@FXML 
	private Text hallOfFameLabel;
	@FXML 
	private Text penaltyTimeSec;
	@FXML 
	private TextField penaltyTime;
	@FXML 
	private TextField maxMistakes;
	@FXML 
	private VBox hallOfFame;
	
	//ALL the CHECKBOXES
	@FXML 
	private GridPane CBoxRow1; //sensors from 1-15
	@FXML 
	private GridPane CBoxRow2; //sensors from 16-29
	
	//InfoPane and lock related
	@FXML 
	private VBox controlPanel;
	@FXML 
	private GridPane infoPane;
	@FXML 
	private Label infoField;
	@FXML 
	private PasswordField passwordField;
	@FXML 
	private Button lockButton;

	//Database deletion
	@FXML
	private Button rmv_last;
	@FXML
	private Button btn_yes;
	@FXML
	private Button btn_no;
	
	@FXML 
	private RadioButton todayButton;
	@FXML 
	private RadioButton weekButton;
	@FXML 
	private RadioButton monthButton;
	@FXML 
	private RadioButton allButton;
	
	@FXML 
	private Slider roomSoundSlider;
	@FXML
	private Slider mistakeSoundSlider;
	
	@FXML 
	private ToggleButton roomOnButton;
	@FXML 
	private ToggleButton roomOffButton;
	@FXML 
	private ToggleButton mistakeOnButton;
	@FXML 
	private ToggleButton mistakeOffButton;

	private Float penalty;
	private Integer mistakes;
	private boolean locked = false;

	public GameMasterViewController(LaserMazeUI laserMazeUI) {
		this.laserMazeUI = laserMazeUI;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Logger.UIlog("Game Master view initialized.");
		unlockScreen();
		//Assigning functions to checkBoxes
		for(Node temp : CBoxRow1.getChildren()){
			CheckBox tempike = (CheckBox)temp;
			tempike.setStyle("fx-text-fill:#ffff00");
			tempike.setOnAction(new EventHandler<ActionEvent>(){
				@Override
				public void handle(ActionEvent event) {
					setSensorUsage(tempike);
				}
			});
		}
		for(Node temp : CBoxRow2.getChildren()){
			CheckBox tempike = (CheckBox)temp;
			tempike.setOnAction(new EventHandler<ActionEvent>(){
				@Override
				public void handle(ActionEvent event) {
					setSensorUsage(tempike);
				}
			});
		}
		
		// Create slider listeners.
		roomSoundSlider.valueProperty().addListener(ev -> updateRoomSoundVolume());
		mistakeSoundSlider.valueProperty().addListener(ev -> updateMistakeSoundVolume());
	}
	
	/**
	 * @usage Change player name field value.
	 * @param name New player's name.
	 */
	public void changeName(String name) {
		Logger.UIlog("Name changed: " + name);
		playersName.setText(Globals.name);
	}
	
	/**
	 * @usage Change start, stop and pause button colors. Also gamestate style.
	 */
	public void changeStartCol() {
		startButton.setStyle("-fx-background-color: #A5A5A5");
        stopButton.setStyle("-fx-background-color: #CB3827");
        pauseButton.setStyle("-fx-background-color: #D9A519");
        gameState.setStyle("-fx-fill: #30B534");
        
        try {
			for (Node label : Arrays.asList(penaltyTimeLabel, maxMistakesLabel, hallOfFameLabel, penaltyTimeSec, hallOfFame)) {
				label.setStyle("-fx-opacity: 0.5");
			}
		} catch (Exception e) {
        	Logger.UIlog("Error while changing start color: " + e);
		}

        for (TextField textField : Arrays.asList(penaltyTime, maxMistakes)) {
        	textField.setEditable(false);
        	textField.setStyle("-fx-opacity: 0.5");
		}
        
        startButton.setDisable(true);
		stopButton.setDisable(false);
		pauseButton.setDisable(false);
	}
	/**
	 * @usage Disables fields penaltyTime, maxMistakes, buttons. Edits style elements.
	 * @param event
	 */
	@FXML 
	protected void startButtonClicked(ActionEvent event) {
		//if(laserMazeUI.getAudienceViewState() != AudienceViewState.LIVE_VIDEO_VIEW) return;
		laserMazeUI.startGame();
		changeStartCol();
	}
	
	public void changeStopStyle() {
		startButton.setStyle("-fx-background-color: #036506");
        stopButton.setStyle("-fx-background-color: #A5A5A5");
        pauseButton.setStyle("-fx-background-color: #A5A5A5");
        gameState.setStyle("-fx-fill: #CB3827");
		
        for (Node label : Arrays.asList(penaltyTimeLabel, maxMistakesLabel, hallOfFameLabel, penaltyTime, hallOfFame)) {
			label.setStyle("-fx-opacity: 1");
		}
		
		for (TextField textField : Arrays.asList(penaltyTime, maxMistakes)) {
			textField.setEditable(true);
			textField.setStyle("-fx-opacity: 1");
		}
		
		startButton.setDisable(false);
		stopButton.setDisable(true);
		pauseButton.setDisable(true);
	}
	
	
	/**
	 * @usage Activates fields penaltyTime, maxMistakes, buttons. Edits style elements.
	 * @param event
	 */
    @FXML 
    protected void stopButtonClicked(ActionEvent event) {
        laserMazeUI.endGame();
        changeStopStyle();
        
    }
    
    
    public void changePauseStyle() {
        startButton.setStyle("-fx-background-color: #036506;");
        stopButton.setStyle("-fx-background-color: #CB3827;");
        pauseButton.setStyle("-fx-background-color: #A5A5A5;");
        gameState.setStyle("-fx-fill: #D9A519");
		startButton.setDisable(false);
		stopButton.setDisable(false);
		pauseButton.setDisable(true);
    }
    
    /**
     * @usage Disables/enables buttons. Edits style elements.
     * @param event
     */
	@FXML 
	protected void pauseButtonClicked(ActionEvent event) {
		laserMazeUI.pauseGame();
		changePauseStyle();
	}
	
	/**
	 * @usage Save new settings game master has made.
	 * @param event
	 */
	@FXML 
	protected void saveSettings(ActionEvent event) {
		try {
			//If a user starts editing the field but doesn't enter a value the default value will be used.
			if (Objects.equals(penaltyTime.getText(), "")) {
				penaltyTime.getStyleClass().remove("gm-num-text-field-error");
				Logger.communicatorLog("Vajutati nuppu Salvesta, kuid väärtust 'Trahv' ei lisatud. Väärtuseks default väärtus.");
			//User entered a value. Let's control if it's a numerical value. If it is not, an exception is thrown.
			} else {
				penalty = Float.parseFloat(penaltyTime.getText());
				PCLogic.setPenaltyTime(penalty);
				penaltyTime.getStyleClass().remove("gm-num-text-field-error");
			} penaltyTime.getStyleClass().remove("gm-num-text-field-error");
		} catch (Exception e) {
			Logger.communicatorLog("Sisestatud väärtus peab olema number! Tehniline veateade: " + e.toString());
			penaltyTime.getStyleClass().add("gm-num-text-field-error");
		} try {
			if (Objects.equals(maxMistakes.getText(), "")) {
				maxMistakes.getStyleClass().remove("gm-num-text-field-error");
				Logger.communicatorLog("Vajutati nuppu Salvesta, kuid väärtust 'Lubatud vigu' ei lisatud. Väärtuseks default väärtus.");
			} else {
				mistakes = Integer.parseInt(maxMistakes.getText());
				PCLogic.setMaxMistakes(mistakes);
				maxMistakes.getStyleClass().remove("gm-num-text-field-error");
				} maxMistakes.getStyleClass().remove("gm-num-text-field-error");
		} catch (Exception e) {
			Logger.communicatorLog("Sisestatud väärtus peab olema täisarv! Tehniline veateade: " + e.toString());
			maxMistakes.getStyleClass().add("gm-num-text-field-error");
		}
		this.laserMazeUI.setSecondaryStageCanGetFocus(true);
		Logger.UIlog("New settings saved.");
	}
	
	@FXML
	/**
	 * @usage Request focus for game master view.
	 */
	private void requestFocusForGameMasterView()
	{
		this.laserMazeUI.setSecondaryStageCanGetFocus(false);
	}

	/**
	 * @usage Show if sensor is selected.
	 * @param itself Sensor itself.
	 */
	protected void setSensorUsage(CheckBox itself) {
		int id = Integer.parseInt(itself.getText().substring(0, 2));
		laserMazeUI.setSensorUsage(id, itself.isSelected());
	}
	
	/**
	 * @usage Set game state text field value.
	 * @param input What game status is.
	 */
    protected void setGameStateText(String input) {
		Platform.runLater(new Runnable() {
			@Override public void run() { gameState.setText(input); }
		});
    }
    
    /**
     * @usage Reset all sensor mistake values.
     */
    protected void resetSensors() {
    	Platform.runLater(new Runnable() {
            @Override public void run() {
                for(Node a : CBoxRow1.getChildren()){
                    CheckBox temp = (CheckBox)a;
                    temp.setText(temp.getText().split( " - ")[0]+" - " + 0 + " viga");
                }
                for(Node a : CBoxRow2.getChildren()){
                    CheckBox temp = (CheckBox)a;
                    temp.setText(temp.getText().split( " - ")[0]+" - " + 0 + " viga");
                }
            }
        });
	}
    
    /**
     * @usage Set mistake value for specific sensor and also change color of its label.
     * @param id Sensor id.
     * @param val Value to set for sensor.
     * @param mistakes Value of mistakes for a sensor.
     */
    protected void setMistakesForSensor(int id, boolean val, int mistakes) {
		Platform.runLater(new Runnable() {
			@Override public void run() {
				if(id <=15){
					CheckBox temp = (CheckBox) CBoxRow1.getChildren().get(id-1);
					temp.setText(temp.getText().split( " - ")[0]+ " - " + mistakes + " viga");
					if(val){
						temp.setStyle("-fx-text-fill:white");
					}
					else{
						temp.setStyle("-fx-text-fill:red");
					}
				}
				else{
					CheckBox temp = (CheckBox) CBoxRow2.getChildren().get(id-16);
					temp.setText(temp.getText().split(" - ")[0]+ " - " + mistakes + " viga");
					if(val) {
						temp.setStyle("-fx-text-fill:white");
					}
					else{
						temp.setStyle("-fx-text-fill:red");
					}
				}
			}
		});
	}
    
	/**
	 * @usage Check if user entered correct password.
	 * @param event
	 */
	@FXML
	private void passwordEntering(KeyEvent event) {
		if(event.getCode().equals(KeyCode.ENTER)) {
			//Maybe should be better password check..
			if(passwordField.getText().equals("utphotonics")) {
				unlockScreen();
			}
		}
	}

	/**
	 * @usage Displays the decision on deleting the last result
	 * @param event
	 */
	@FXML
	protected void deleteResult(ActionEvent event) {
		enableinfoField(true, true, false); 
	}

	/**
	 * @usage Deletes the last added result from the database.
	 * @param event
	 */
	@FXML
	protected void deleteLast(ActionEvent event) {
		this.laserMazeUI.getPCLogic().getDbController().deleteLastGame();
		this.laserMazeUI.getHOFViewController().update();
		enableinfoField(false, false, false);
	}

	/**
	 * @usage If the GM wants to still keep the last result.
	 * @param event
	 */
	@FXML
	protected void keepLast(ActionEvent event) { 
		enableinfoField(false, false, false);
	}

	/**
	 * @usage Locks game master view.
	 * @param event
	 */
	@FXML
	protected void lockScreen(ActionEvent event) {
		this.laserMazeUI.setSecondaryStageCanGetFocus(false);
		enableinfoField(true, false, true);
	}
	
	/**
	 * @usage Unlocks game master view.
	 */
	private void unlockScreen() {
		this.laserMazeUI.setSecondaryStageCanGetFocus(true);
		enableinfoField(false, false, false);
	}
	
	/**
	 * @usage Enable info fields for Ardino errors, password entering and last game delteing.
	 * @param setActive Set this view active.
	 * @param deleting Is the method used for deleting.
	 * @param activatePassword Is the method used for activating password.
	 */
	private void enableinfoField(boolean setActive, boolean deleting, boolean activatePassword) {
		if(setActive) {
			infoField.setVisible(true);
            infoPane.setVisible(true);
            btn_no.setVisible(false);
        	btn_yes.setVisible(false);
            if(activatePassword) {
            	infoField.setText("Parool: ");
                infoField.setTextAlignment(TextAlignment.LEFT);
                passwordField.setDisable(false);
                locked = true;
            }else if(deleting){
            	infoField.setText("Oled Sa kindel?");
            	infoField.setTextAlignment(TextAlignment.LEFT);
            	btn_no.setVisible(true);
            	btn_yes.setVisible(true);
			}else {
            	infoField.setText("Ei saa ühendust Arduinoga!");
                infoField.setTextAlignment(TextAlignment.CENTER);
                passwordField.setDisable(true);
            }
            controlPanel.setEffect(new GaussianBlur());
		}else {
			infoField.setVisible(false);
			infoPane.setVisible(false);
			btn_no.setVisible(false);
			btn_yes.setVisible(false);
			passwordField.setDisable(true);
			passwordField.setText("");
			controlPanel.setEffect(null);
			locked = false;
		}
	}

	/**
	 * @usage Response to Arduino connection/disconnection
	 */
	public void arduinoConnected() {
        Platform.runLater(new Runnable() {
            @Override public void run() {
            	if(locked) {
            		enableinfoField(true, false, true);
            	}else {
            		enableinfoField(false, false, false);
            	}
            }
        });

    }
    
	/**
	 * @usage Show error message when Arduino is disconnected.
	 */
	public void arduinoDisconnected() {
        Platform.runLater(new Runnable() {
            @Override public void run() {
                enableinfoField(true, false,false);
            }
        });

    }
	
	/**
	 * @usage View scores from today.
	 */
	@FXML
	private void viewTodayScore() {
		todayButton.setSelected(true);
		weekButton.setSelected(false);
		monthButton.setSelected(false);
		allButton.setSelected(false);
		laserMazeUI.getHOFViewController().updateTimeframe("today");
	}
	
	/**
	 * @usage View top 10 scores from entire week.
	 */
	@FXML
	private void viewWeekScore() {
		todayButton.setSelected(false);
		weekButton.setSelected(true);
		monthButton.setSelected(false);
		allButton.setSelected(false);
		laserMazeUI.getHOFViewController().updateTimeframe("thisWeek");
	}
	
	/**
	 * @usage View top 10 scores from entire month.
	 */
	@FXML
	private void viewMonthScore() {
		todayButton.setSelected(false);
		weekButton.setSelected(false);
		monthButton.setSelected(true);
		allButton.setSelected(false);
		laserMazeUI.getHOFViewController().updateTimeframe("thisMonth");
	}
	
	/**
	 * @usage View all top 10 scores from database.
	 */
	@FXML
	private void viewAllScore() {
		todayButton.setSelected(false);
		weekButton.setSelected(false);
		monthButton.setSelected(false);
		allButton.setSelected(true);
		laserMazeUI.getHOFViewController().updateTimeframe("all");
	}
	
	/**
	 * @usage Called when room sound volume slider value is changed. Update room sound volume accordingly to room sound slider value.
	 */
	private void updateRoomSoundVolume() {
		Sound.changeVolume(Sounds.ambient, (float) roomSoundSlider.getValue());
	}
	
	/**
	 * @usage Called when mistakes and other sounds volume slider value is changed. Update mistakes and other sounds volume accordingly to mistakes and other sounds slider value.
	 */
	private void updateMistakeSoundVolume() {
		Sound.changeOtherVolumes((float) mistakeSoundSlider.getValue());
	}
	
	/**
	 * @usage Deactivate mistake sounds and other sounds.
	 */
	@FXML
	private void mistakeMusicOn() {
		Sound.setPlayOtherSounds(true);
	}
	
	/**
	 * @usage Activate mistake sounds and other sounds.
	 */
	@FXML
	private void mistakeMusicOff() {
		Sound.setPlayOtherSounds(false);
	}
	
	/**
	 * @usage Activate room music sound.
	 */
	@FXML
	private void roomMusicOn() {
		Sound.setPlayAmbientSound(true);
		Sound.playSound(Sounds.ambient, true);
	}
	
	/**
	 * @usage Deactivate room music sound.
	 */
	@FXML
	private void roomMusicOff() {
		Sound.setPlayAmbientSound(false);
		Sound.stopSound(Sounds.ambient);
	}
	
	/**
	 * @usage Choose room ambient sound file.
	 */
	@FXML
	private void chooseFilePathForRoomSound() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Vali ruumiheli");
		Sound.stopSound(Sounds.ambient);
		this.laserMazeUI.setSecondaryStageCanGetFocus(false);
		try {
			Sound.changeSoundPath("ambient", fileChooser.showOpenDialog(this.laserMazeUI.getPrimaryStage()).getAbsolutePath());
		}
		catch(NullPointerException e) {}
		this.laserMazeUI.setSecondaryStageCanGetFocus(true);
		Sound.playSound(Sounds.ambient, true);
	}
	
	/**
	 * @usage Choose mistake sound file.
	 */
	@FXML
	private void chooseFilePathForMistakeSound() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Vali veaheli");
		Sound.stopSound(Sounds.damage);
		this.laserMazeUI.setSecondaryStageCanGetFocus(false);
		try {
			Sound.changeSoundPath("klaxon1", fileChooser.showOpenDialog(this.laserMazeUI.getPrimaryStage()).getAbsolutePath());
		}
		catch(NullPointerException e) {}
		this.laserMazeUI.setSecondaryStageCanGetFocus(true);
	}
}