package lm_application;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import lm_main.*;

/**
 * @author Team Laser Maze
 * @date 03.10.2018
 * @usage Laser Maze main UI class.
 */
public class LaserMazeUI extends Application implements UI {
	
	private GameMasterViewController gameMasterViewController;
	private PlayerViewController playerViewController;
	private PromoVideoViewController promoVideoViewController;
	private LiveViewController liveViewController;
	private HoFViewController hofViewController;
	
	private Timeline promoAndHofChangeTimeline;
	
	private PCLogic pcLogic;
	
	private Stage primaryStage;
	private Stage secondaryStage;
	private Scene secondaryScene;
	private HashMap<String, Pane> screens = new HashMap<String, Pane>();
	
	private double primaryScreenWidth;
	private double primaryScreenHeight;
	
	private double secondaryScreenWidth;
	private double secondaryScreenHeight;
	
	private AudienceViewState audienceViewState = AudienceViewState.PROMO_VIDEO_VIEW;
	
	private double timeToShowPromoVideoAndHOF;
	private boolean secondaryStageCanGetFocus;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Logger.UIlog("Laser Maze 2018,\nCarola Kesküla, Raigo Kõvask, Ott Saar, Daniel Nael");
		Logger.UIlog("javafx version: " + System.getProperty("javafx.runtime.version"));
		Logger.UIlog("Application started.");
		
		this.primaryStage = primaryStage;
		this.secondaryStageCanGetFocus = true;
		
		// Audience view state is showing hall of fame view.
		audienceViewState = AudienceViewState.HOF_VIEW;
				
		// Setting how many seconds hof and promo video is shown.
		timeToShowPromoVideoAndHOF = 25;
		
		// Getting primary screen width and height.
		Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
		primaryScreenWidth = bounds.getWidth();
		primaryScreenHeight = bounds.getHeight();
		

		// Create controllers.
		gameMasterViewController = new GameMasterViewController(this);
		playerViewController = new PlayerViewController(this);
		liveViewController = new LiveViewController(this);
		promoVideoViewController = new PromoVideoViewController(this);
		hofViewController = new HoFViewController(this);

		// Create stages.
		primaryStage = createStage("/GameMasterView.fxml", gameMasterViewController, primaryScreenWidth, primaryScreenHeight, "Game Master's View");
		secondaryStage = new Stage();

        pcLogic = new PCLogic(this);
		
		// Create screens.
		screens.put("player", createRoot("/PlayerView.fxml", playerViewController, secondaryScreenWidth, secondaryScreenHeight));
		screens.put("hof", createRoot("/hofView.fxml", hofViewController, secondaryScreenWidth, secondaryScreenHeight));
		screens.put("promo", createRoot("/PromoVideoView.fxml", promoVideoViewController, secondaryScreenWidth, secondaryScreenHeight));
		screens.put("liveView", createRoot("/LiveView.fxml", liveViewController, secondaryScreenWidth, secondaryScreenHeight));
				
		// Create secondary scene, add root to it and add it to secondaryStage.
		secondaryScene = new Scene(screens.get("hof"), secondaryScreenWidth, secondaryScreenHeight);
		secondaryStage.setScene(secondaryScene);
		
		// Scene management.
		secondaryScene.setOnKeyPressed(ev -> { 
			if(pcLogic.getGameStatus().equals("Stopped") && (audienceViewState == AudienceViewState.HOF_VIEW || audienceViewState == AudienceViewState.PROMO_VIDEO_VIEW)) {
				setScreen("player");
				promoVideoViewController.stopPromoVideo();				
				promoAndHofChangeTimeline.stop();
				audienceViewState = AudienceViewState.LIVE_VIDEO_VIEW;
			}
			else if(audienceViewState == AudienceViewState.LIVE_VIDEO_VIEW && pcLogic.getGameStatus().equals("Stopped")) {
				setScreen("player");
			}
		});
		

		// Set up stages. Send secondary stage to different monitor.
		sendStageToDifferentMonitor(secondaryStage);
		secondaryStage.setTitle("Audience View");
		secondaryStage.setFullScreen(true);
		
		primaryStage.setWidth(primaryScreenWidth);
		primaryStage.setHeight(primaryScreenHeight);
		primaryStage.setMaximized(true);

		// Show stages.
		primaryStage.show();
		secondaryStage.show();
		
		//primaryStage.setFullScreen(true);

		liveViewController.activate();
		
		// Change promo video view and hof view.
		promoAndHofChangeTimeline = new Timeline(new KeyFrame(Duration.seconds(timeToShowPromoVideoAndHOF), ae -> {
			try {
				changeAudienceView();
			} 
			catch (IOException | LaserMazeException e) {
				e.printStackTrace();
			}
		}));
		
		promoAndHofChangeTimeline.setCycleCount(Animation.INDEFINITE);
		promoAndHofChangeTimeline.play();
		
		Sound.playSound(Sound.Sounds.ambient, true);
		primaryStage.setOnCloseRequest(ev -> {pcLogic.close();});
		
		Timeline focusRequestTimeline = new Timeline(new KeyFrame(Duration.seconds(0.01), ae -> {
			if(secondaryStageCanGetFocus) {
				if(!secondaryStage.isFocused()) { 
					secondaryStage.requestFocus();
				}
			}
		}));
		
		focusRequestTimeline.setCycleCount(Animation.INDEFINITE);
		focusRequestTimeline.play();
	}
	
	/**
	 * @usage Change audience view scene graph.
	 * @throws IOException
	 * @throws LaserMazeException
	 */
	private void changeAudienceView() throws IOException, LaserMazeException{
		switch(audienceViewState) {
			case PROMO_VIDEO_VIEW:
				audienceViewState = AudienceViewState.HOF_VIEW;
				secondaryScene.setRoot(screens.get("hof"));
				break;
			case HOF_VIEW:
				audienceViewState = AudienceViewState.PROMO_VIDEO_VIEW;
				secondaryScene.setRoot(screens.get("promo"));
				break;
			default:
				break;
		}
	}
	
	
	/**
	 * @usage Send player's view to other monitor if there is any connected.
	 * @param stage Stage instance to send to additional monitor.
	 */
	private void sendStageToDifferentMonitor(Stage stage) {
		for(int i = 0; i < Screen.getScreens().size(); i++) {
			if(i > 0) {
				Rectangle2D bounds = Screen.getScreens().get(i).getBounds();
				stage.setX(bounds.getMinX());
				stage.setY(bounds.getMinY());
			}
		}
	}
	
	/**
	 * @usage Normalizes everything on audience view when game is over. Shows hall of fame view.
	 */
	public void normalizeState() {
		audienceViewState = AudienceViewState.HOF_VIEW;
		setScreen("hof");
		promoVideoViewController.startPromoVideo();
		promoAndHofChangeTimeline.playFromStart();
		playerViewController.clearName();
		liveViewController.clearTextTime();
		hofViewController.update();
	}
	
	/**
	 * Set up stages easily.
	 * @param fxml Path of an .fxml file.
	 * @param controller Controller for fxml file.
	 * @param width Stage scene width.
	 * @param height Stage scene height.
	 * @param stageName Name of the stage.
	 * @return Stage with all necessary components.
	 * @throws IOException
	 * @throws LaserMazeException
	 */
	private Stage createStage(String fxml, Initializable controller, double width, double height, String stageName) throws IOException, LaserMazeException
	{
		Stage stage = new Stage();
		Scene scene = new Scene(createRoot(fxml, controller, width, height), width, height);
		stage.setTitle(stageName);
		stage.setScene(scene);
		return stage;
	}
	
	/**
	 * Create root for scene.
	 * @param fxml Path of an .fxml file.
	 * @param controller Controller for fxml file.
	 * @param width Stage scene width.
	 * @param height Stage scene height.
	 * @return
	 * @throws IOException
	 * @throws LaserMazeException
	 */
	private Pane createRoot(String fxml, Initializable controller, double width, double height) throws IOException, LaserMazeException {
		URL resource = getClass().getResource(fxml);
		if(resource == null) throw new LaserMazeException("Can't find: " + fxml);
		FXMLLoader fxmlLoader = new FXMLLoader(resource);
		fxmlLoader.setController(controller);
		Pane root = fxmlLoader.load();
		return root;
	}
	
	/**
	 * @usage Set secondary screen scene graph.
	 * @param screenName What screen to show.
	 */
	public void setScreen(String screenName) {
		secondaryScene.setRoot(screens.get(screenName));
	}

	/**
	 * @usage Assigns player's name to related fields in game master and live video controller.
	 * @param playersName Player's name to assing.
	 */
	public void setPlayersName(String playersName)
	{
		PCLogic.setPlayerName(playersName);
		gameMasterViewController.changeName(playersName);
		liveViewController.setPlayerName(playersName);
		liveViewController.resetMistakes();
	}
	
	/**
	 * Set max mistakes from UI.
	 * @param maxMistakes Max mistakes to set.
	 */
	public void setMaxMistakes(int maxMistakes) { 
		PCLogic.setMaxMistakes(maxMistakes); 
	}
	
	/**
	 * Set penalty time from UI.
	 * @param penaltyTime Penalty time to set.
	 */
	public void setPenaltyTime(long penaltyTime){ 
		PCLogic.setPenaltyTime(penaltyTime);
	}
	
	/**
	 * Set sensor usage from UI.
	 * @param id Sensor id.
	 * @param usage Is sensor used.
	 */
	protected void setSensorUsage(int id, boolean usage) { 
		pcLogic.setSensorInGameUsage(id,usage); 
	}
	
	/**
	 * Start the game from UI.
	 */
	protected void startGame() {
		
		audienceViewState = AudienceViewState.LIVE_VIDEO_VIEW;
		setScreen("liveView");
		promoAndHofChangeTimeline.stop();
		promoVideoViewController.stopPromoVideo();
		
		pcLogic.startGame();
	}
	
	/**
	 * End the game from UI.
	 */
	protected void endGame() {
		pcLogic.endGame();
	}
	
	/**
	 * Pause the game from UI.
	 */
	protected void pauseGame() { 
		pcLogic.pauseGame(); 
	}
	
	/**
	 * Set current time and game state in game master and live view controller.
	 */
	public void setTime() {
		switch (pcLogic.getGameStatus()) {
			case "Running":
				gameMasterViewController.setGameStateText("MÄNG KÄIB: " + pcLogic.getGameTimeAsString());
				if(pcLogic.getMadeMistakes() == 0){
				    gameMasterViewController.resetSensors();
                }
				liveViewController.setTextTime(pcLogic.getGameTimeAsString());
				break;
			case "Paused":
				gameMasterViewController.setGameStateText("MÄNG PAUSIL: " + pcLogic.getGameTimeAsString());
				liveViewController.setTextTime(pcLogic.getGameTimeAsString());
				break;
			default:
				gameMasterViewController.setGameStateText("MÄNG LÄBI: " + pcLogic.getGameTimeAsString());
				liveViewController.setTextTime(pcLogic.getGameTimeAsString());
				break;
		}
	}
	
	/**
	 * Tell game master view controller if Arduino is disconnected or connected.
	 */
	@Override
	public void arduinoDisconnected(boolean value)
	{
	    //COMMENT THIS OUT IF U HAVE NO ARDUINO AND WANT TO TEST GM
		if(value){
			pauseGame();
			gameMasterViewController.arduinoDisconnected();
		}
		else{
			gameMasterViewController.arduinoConnected();
		}
	}
	
	/**
	 * Set specific sensor values in game master view controller and in live view controller set how many mistakes has player made.
	 */
	@Override
	public void setSensor(int sensorID, boolean currentVal, int currentMistakes) {
		gameMasterViewController.setMistakesForSensor(sensorID-21, currentVal, currentMistakes);
		liveViewController.setTextMistakes(pcLogic.getMadeMistakes());
	}
	
	/**
	 * @usage Send web camera image to live view controller.
	 */
	@Override
	public void sendCamInfo(Image img) {
		liveViewController.setCameraImage(img);
	}
	
	public PCLogic getPCLogic() { 
		return this.pcLogic; 
	}
	
	public HoFViewController getHOFViewController() { 
		return this.hofViewController; 
	}
	
	public Stage getSecondaryStage() {
		return secondaryStage;
	}
	
	public AudienceViewState getAudienceViewState() {
		return this.audienceViewState;
	}

	@Override
	public void gameStopped() {
		gameMasterViewController.changeStopStyle();
		
	}

	@Override
	public void gamePaused() {
		gameMasterViewController.changePauseStyle();
		
	}

	@Override
	public void gameStarted() {
		gameMasterViewController.changeStartCol();
		audienceViewState = AudienceViewState.LIVE_VIDEO_VIEW;
		setScreen("liveView");
		promoAndHofChangeTimeline.stop();
		promoVideoViewController.stopPromoVideo();
		
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}
	
	public boolean getSecondaryStageCanGetFocus() {
		return this.secondaryStageCanGetFocus;
	}
	
	public void setSecondaryStageCanGetFocus(boolean value) {
		this.secondaryStageCanGetFocus = value;
	}
}