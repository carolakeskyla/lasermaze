#include <Wire.h>
#include <SD.h>
#include <SPI.h>
/**
 * lambda function, that sends info about detector.
 * @param i detector pin number.
 */
auto detector_status = [](byte i) 
    {
      if(i < 10){
        SerialUSB.print("b");
      }else{
        SerialUSB.print("v"); 
      }
      SerialUSB.print(i);
      SerialUSB.print(digitalRead(i));
    };
/* Arrays, where are button and detector pin numbers */
static const byte buttons[] = {2,3};
static const byte detectors[] = {22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50};
void setup() {
 SerialUSB.begin(9600);
 for(int n = 0; n < sizeof(detectors); n++){
  pinMode(detectors[n], INPUT);
 }
 for(int n = 0; n < sizeof(buttons); n++){
  pinMode(buttons[n], INPUT);
 }
  // ------------------------------INIT SERIAL-----------------------------------------------
 /* // ------------------------------INIT AUDIO-----------------------------------------------
    // setup SD-card
  Serial.print("Initializing SD card...");
  if (!SD.begin(9)) {
    Serial.println(" failed!");
    while(true);
  }
  Serial.println(" done.");
  // hi-speed SPI transfers
  // 44100kHz stereo => 88200 sample rate
  // 100 mSec of prebuffering.
  Audio.begin(88200, 100);*/
  // ------------------------------INIT GPIO-----------------------------------------------
  //pinMode(ledPin, OUTPUT);
 
  // ------------------------------INIT DETECTORS------------------------------------------
  attachInterrupt(digitalPinToInterrupt(buttons[0]), [] () {detector_status(buttons[0]);} , FALLING);
  attachInterrupt(digitalPinToInterrupt(buttons[1]), [] () {detector_status(buttons[1]);} , FALLING);
  attachInterrupt(digitalPinToInterrupt(detectors[0]), [] () {detector_status(detectors[0]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[1]), [] () {detector_status(detectors[1]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[2]), [] () {detector_status(detectors[2]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[3]), [] () {detector_status(detectors[3]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[4]), [] () {detector_status(detectors[4]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[5]), [] () {detector_status(detectors[5]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[6]), [] () {detector_status(detectors[6]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[7]), [] () {detector_status(detectors[7]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[8]), [] () {detector_status(detectors[8]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[9]), [] () {detector_status(detectors[9]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[10]), [] () {detector_status(detectors[10]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[11]), [] () {detector_status(detectors[11]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[12]), [] () {detector_status(detectors[12]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[13]), [] () {detector_status(detectors[13]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[14]), [] () {detector_status(detectors[14]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[15]), [] () {detector_status(detectors[15]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[16]), [] () {detector_status(detectors[16]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[17]), [] () {detector_status(detectors[17]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[18]), [] () {detector_status(detectors[18]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[19]), [] () {detector_status(detectors[19]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[20]), [] () {detector_status(detectors[20]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[21]), [] () {detector_status(detectors[21]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[22]), [] () {detector_status(detectors[22]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[23]), [] () {detector_status(detectors[23]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[24]), [] () {detector_status(detectors[24]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[25]), [] () {detector_status(detectors[25]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[26]), [] () {detector_status(detectors[26]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[27]), [] () {detector_status(detectors[27]);} , CHANGE);
  attachInterrupt(digitalPinToInterrupt(detectors[28]), [] () {detector_status(detectors[28]);} , CHANGE);
}
void loop(){
    //for sending information about all detectors
    if(SerialUSB.available()){
      char read = SerialUSB.read();
      if(read == 'a'){
        for(int i = 0;i<sizeof(detectors);i++){
          detector_status(detectors[i]);}
      }
      if(read == 'c') {
        SerialUSB.print(read);
      }
   }
}
